# spring-boot
## Pre-requisite:

1. Framework: Spring Boot v2.0.4
2. Java 8
3. Thymeleaf
4. Bootstrap v.4.3.1
5.Maven 4.0

    
## Deploying with Intellij IDE
1. Create project from Initializr: File > New > project > Spring Initializr
2. Add lib repository into pom.xml
3. Add folders
    - Source root: src/main/java
    - View: src/main/resources
    - Static: src/main/resource/static

4. use maven command : " mvn -clean " and " mvn -install"
5. Create database with name "demo" as configuration in application.properties
6. Run sql script to create table doc/data.sql
7. run the Application in src/main/java/com/nnk/springboot

## Building with maven
 Go to yout local repository and clean and package the files, plug-ins, and libraries before running the application:
 
 "mvn clean package"

## Running with Maven
Use the Maven Local repository to run Application:
 
 " mvn exec:java -Dexec.Application="com.nnk.springboot.Application" " 

## Implement a Feature
1. Create mapping domain class and place in package com.nnk.springboot.domain
2. Create repository class and place in package com.nnk.springboot.repositories
3. Create controller class and place in package com.nnk.springboot.controllers
4. Create view files and place in src/main/resource/templates

## Write Unit Test
1. Create unit test and place in package com.nnk.springboot in folder test > java

## Security
1. Create user service to load user from  database and place in package com.nnk.springboot.services
2. Add configuration class and place in package com.nnk.springboot.config
