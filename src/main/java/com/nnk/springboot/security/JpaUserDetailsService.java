package com.nnk.springboot.security;

import com.nnk.springboot.repositories.UserRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * The type Jpa user details service.
 */
@Service
public class JpaUserDetailsService implements UserDetailsService {

    /**
     * @see UserRepository
     */
    @Autowired
    private UserRepository userRepository;

    /**
     * @see Logger
     */
    private static final Logger LOGGER = LogManager.getLogger(
            JpaUserDetailsService.class);

    /**
     *{@inheritDoc}
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        LOGGER.info("Getting user info via JPA");
        return userRepository.findByUsername(username)
                .orElseThrow(() -> {
                    return new UsernameNotFoundException(
                            "Username " + username + " not found");
                });
    }
}
