package com.nnk.springboot.exceptions;

/**
 * The type User already existing exception.
 */
public class UserAlreadyExistingException extends Exception {

    /**
     * Instantiates a new User already existing exception.
     *
     * @param message the message
     */
    public UserAlreadyExistingException(String message) {
        super(message);
    }
}

