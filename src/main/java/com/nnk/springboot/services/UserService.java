package com.nnk.springboot.services;

import com.nnk.springboot.domain.User;
import com.nnk.springboot.exceptions.UserAlreadyExistingException;
import com.nnk.springboot.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import javax.persistence.EntityNotFoundException;
import java.util.List;

/**
 * The type User service.
 */
@Service
public class UserService {

    /**
     * @see UserRepository
     */
    @Autowired
    private UserRepository userRepository;

    /**
     * @see PasswordEncoder
     */
    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     * Find all user.
     *
     * @return the user list
     */
    public List<User> findAll() {
        return userRepository.findAll();
    }

    /**
     * Save user.
     *
     * @param user the user to save
     * @return the user saved
     * @throws UserAlreadyExistingException if an user with the same username exists
     */
    public User save(User user) throws UserAlreadyExistingException {

        if (userRepository.existsByUsername(user.getUsername())) {
            throw new UserAlreadyExistingException("User with username " + user.getUsername() + " is already existing");
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

    /**
     * Update user.
     *
     * @param user the user to update
     * @return the user updated
     * @throws UserAlreadyExistingException if a user with the same username exists and if the user's username to
     * update is not equals with the user's id to update found in db
     */
    public User update(User user) throws UserAlreadyExistingException {

        if (userRepository.existsByUsername(user.getUsername()) && (!user.getUsername()
                .equals(userRepository.findById(user.getId())
                                .get()
                                .getUsername()))) {
            throw new UserAlreadyExistingException("User with username " + user.getUsername() + " is already existing");
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }


    /**
     * Find user by id.
     *
     * @param id the user id
     * @return the user
     */
    public User findById(int id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Entity doesn't exists"));
    }

    /**
     * Delete user by id.
     *
     * @param id the user id to delete
     */
    public void deleteById(int id) {
        userRepository.deleteById(id);
    }

}
