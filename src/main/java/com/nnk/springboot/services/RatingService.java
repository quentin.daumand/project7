package com.nnk.springboot.services;

import com.nnk.springboot.domain.Rating;
import com.nnk.springboot.repositories.RatingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.persistence.EntityNotFoundException;
import java.util.List;

/**
 * The type Rating service.
 */
@Service
public class RatingService {

    /**
     * @see RatingRepository
     */
    @Autowired
    private RatingRepository ratingRepository;

    /**
     * Find all rating.
     *
     * @return the rating list
     */
    public List<Rating> findAll() {
        return ratingRepository.findAll();
    }

    /**
     * Save rating.
     *
     * @param rating the rating to save
     * @return the rating saved
     */
    public Rating save(Rating rating) {
        return ratingRepository.save(rating);
    }

    /**
     * Find rating by id.
     *
     * @param id the rating id
     * @return the rating
     */
    public Rating findById(int id) {
        return ratingRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Entity doesn't exists"));
    }

    /**
     * Delete rating by id.
     *
     * @param id the rating id to delete
     */
    public void deleteById(int id) {
        ratingRepository.deleteById(id);
    }

}
