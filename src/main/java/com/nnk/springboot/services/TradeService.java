package com.nnk.springboot.services;

import com.nnk.springboot.domain.Trade;
import com.nnk.springboot.repositories.TradeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.persistence.EntityNotFoundException;
import java.util.List;

/**
 * The type Trade service.
 */
@Service
public class TradeService {

    /**
     * @see TradeRepository
     */
    @Autowired
    private TradeRepository tradeRepository;

    /**
     * Find all trade.
     *
     * @return the trade list
     */
    public List<Trade> findAll() {
        return tradeRepository.findAll();
    }

    /**
     * Save trade.
     *
     * @param trade the trade to save
     * @return the trade saved
     */
    public Trade save(Trade trade) {
        return tradeRepository.save(trade);
    }

    /**
     * Find trade by id.
     *
     * @param id the trade id
     * @return the trade
     */
    public Trade findById(int id) {
        return tradeRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Entity doesn't exists"));
    }

    /**
     * Delete trade by id.
     *
     * @param id the trade id to delete
     */
    public void deleteById(int id) {
        tradeRepository.deleteById(id);
    }

}
