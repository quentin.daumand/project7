package com.nnk.springboot.services;

import com.nnk.springboot.domain.BidList;
import com.nnk.springboot.repositories.BidListRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.persistence.EntityNotFoundException;
import java.util.List;

/**
 * The type Bid list service.
 */
@Service
public class BidListService {

    /**
     * @see BidListRepository
     */
    @Autowired
    private BidListRepository bidListRepository;

    /**
     * Find all bid list.
     *
     * @return bid list list
     */
    public List<BidList> findAll() {
        return bidListRepository.findAll();
    }

    /**
     * Save bid list.
     *
     * @param bidList the bid list to save
     * @return the bid list saved
     */
    public BidList save(BidList bidList) {
        return bidListRepository.save(bidList);
    }

    /**
     * Find bid list by id.
     *
     * @param id the bid list id
     * @return the bid list
     */
    public BidList findById(int id) {
        return bidListRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Entity doesn't exists"));
    }

    /**
     * Delete bid list by id.
     *
     * @param id the bid list id to delete
     */
    public void deleteById(int id) {
        bidListRepository.deleteById(id);
    }

}
