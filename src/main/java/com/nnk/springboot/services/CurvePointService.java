package com.nnk.springboot.services;

import com.nnk.springboot.domain.CurvePoint;
import com.nnk.springboot.repositories.CurvePointRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.persistence.EntityNotFoundException;
import java.util.List;

/**
 * The type Curve point service.
 */
@Service
public class CurvePointService {

    /**
     * @see CurvePointRepository
     */
    @Autowired
    private CurvePointRepository curvePointRepository;

    /**
     * Find all curve point.
     *
     * @return curve point list
     */
    public List<CurvePoint> findAll() {
        return curvePointRepository.findAll();
    }

    /**
     * Save curve point.
     *
     * @param curvePoint the curve point to save
     * @return the curve point saved
     */
    public CurvePoint save(CurvePoint curvePoint) {
        return curvePointRepository.save(curvePoint);
    }

    /**
     * Find curve point by id.
     *
     * @param id the curve point id
     * @return the curve point
     */
    public CurvePoint findById(int id) {
        return curvePointRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Entity doesn't exists"));
    }

    /**
     * Delete curve point by id.
     *
     * @param id the curve point id to delete
     */
    public void deleteById(int id) {
        curvePointRepository.deleteById(id);
    }

}
