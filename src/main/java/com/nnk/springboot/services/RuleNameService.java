package com.nnk.springboot.services;

import com.nnk.springboot.domain.RuleName;
import com.nnk.springboot.repositories.RuleNameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.persistence.EntityNotFoundException;
import java.util.List;

/**
 * The type Rule name service.
 */
@Service
public class RuleNameService {

    /**
     * @see RuleNameRepository
     */
    @Autowired
    private RuleNameRepository ruleNameRepository;

    /**
     * Find all rule name.
     *
     * @return the rule name list
     */
    public List<RuleName> findAll() {
        return ruleNameRepository.findAll();
    }

    /**
     * Save rule name.
     *
     * @param ruleName the rule name to save
     * @return the rule name saved
     */
    public RuleName save(RuleName ruleName) {
        return ruleNameRepository.save(ruleName);
    }

    /**
     * Find rule name by id.
     *
     * @param id the rule name id
     * @return the rule name
     */
    public RuleName findById(int id) {
        return ruleNameRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Entity doesn't exists"));
    }

    /**
     * Delete rule name by id.
     *
     * @param id the rule name id to delete
     */
    public void deleteById(int id) {
        ruleNameRepository.deleteById(id);
    }

}
