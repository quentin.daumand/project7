package com.nnk.springboot.controllers;

import com.nnk.springboot.domain.Trade;
import com.nnk.springboot.services.TradeService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.List;

/**
 * The Trade controller.
 */
@PreAuthorize("hasAnyRole('ADMIN','USER')")
@Controller
public class TradeController {

    /**
     * @see TradeService
     */
    @Autowired
    private TradeService tradeService;

    /**
     * @see Logger
     */
    private static final Logger LOGGER = LogManager.getLogger(
            TradeController.class);

    /**
     * Trade home.
     *
     * @param model     the model
     * @param principal the OAuth2 user authenticated
     * @return redirect to trade list view
     */
    @RequestMapping("/trade/list")
    public String home(Model model, @AuthenticationPrincipal OAuth2User principal) {
        List<Trade> tradeList = tradeService.findAll();
        model.addAttribute("tradeList", tradeList);
        if (principal != null) {
            model.addAttribute("login", principal.getAttribute("login"));
        }
        LOGGER.info("TradeController (home) -> " + tradeList.size() + " found");
        return "trade/list";
    }

    /**
     * Add rating form.
     *
     * @param model the model
     * @return trade add form view
     */
    @GetMapping("/trade/add")
    public String addUser(Model model) {
        model.addAttribute("trade", new Trade());
        LOGGER.info("TradeController (add) -> Accessing add form");
        return "trade/add";
    }

    /**
     * Add trade.
     *
     * @param trade  the trade
     * @param result the result
     * @return either add form view if BindingResult has error or redirect to trade list view if is valid
     */
    @PostMapping("/trade/validate")
    public String validate(@Valid @ModelAttribute("trade") Trade trade, BindingResult result) {
        if (result.hasErrors()) {
            LOGGER.debug("TradeController (validate) -> Invalid fields provided");
            return "trade/add";
        }
        tradeService.save(trade);
        LOGGER.info("TradeController (validate) -> Trade successfully added");
        return "redirect:/trade/list";
    }

    /**
     * Update trade form.
     *
     * @param id    the trade id to update
     * @param model the model
     * @return trade update form view
     */
    @GetMapping("/trade/update/{id}")
    public String showUpdateForm(@PathVariable("id") Integer id, Model model) {
        Trade trade = tradeService.findById(id);
        LOGGER.info("TradeController -> BidList to update found: " + trade.toString());
        model.addAttribute("trade", trade);
        return "trade/update";
    }

    /**
     * Update trade.
     *
     * @param id     the trade id to update
     * @param trade  the trade
     * @param result the result
     * @return either update form view if BindingResult has error or redirect to trade list view if is valid
     */
    @PostMapping("/trade/update/{id}")
    public String updateTrade(@PathVariable("id") Integer id, @Valid Trade trade,
            BindingResult result) {
        if (result.hasErrors()) {
            LOGGER.debug("TradeController (update) -> Invalid fields provided");
            return "trade/update";
        }
        tradeService.save(trade);
        LOGGER.info("TradeController (update) -> Trade successfully updated: " + trade.toString());
        return "redirect:/trade/list";
    }

    /**
     * Delete trade.
     *
     * @param id the trade id to delete
     * @return redirect to trade list form view
     */
    @GetMapping("/trade/delete/{id}")
    public String deleteTrade(@PathVariable("id") Integer id) {
        LOGGER.info("TradeController -> Deleting Trade with id:" + id);
        tradeService.deleteById(id);
        return "redirect:/trade/list";
    }
}
