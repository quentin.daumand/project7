package com.nnk.springboot.controllers;

import com.nnk.springboot.domain.User;
import com.nnk.springboot.exceptions.UserAlreadyExistingException;
import com.nnk.springboot.services.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.List;

/**
 * The  User controller.
 */
@PreAuthorize("hasRole('ADMIN')")
@Controller
public class UserController {

    /**
     * @see UserService
     */
    @Autowired
    private UserService userService;

    /**
     * @see Logger
     */
    private static final Logger LOGGER = LogManager.getLogger(
            UserController.class);

    /**
     * User home.
     *
     * @param model the model
     * @return user list view
     */
    @RequestMapping("/user/list")
    public String home(Model model) {
        List<User> userList = userService.findAll();
        model.addAttribute("userList", userList);
        LOGGER.info("UserController (home) -> " + userList.size() + " found");
        return "user/list";
    }

    /**
     * Add user form.
     *
     * @param model the model
     * @return user add form view
     */
    @GetMapping("/user/add")
    public String addUser(Model model) {
        model.addAttribute("user", new User());
        LOGGER.info("UserController (add) -> Accessing add form");
        return "user/add";
    }

    /**
     * Add user.
     *
     * @param user   the user
     * @param result the result
     * @param model  the model
     * @return either add form view if BindingResult has error or redirect to user list view if is valid
     */
    @PostMapping("/user/validate")
    public String validate(@Valid @ModelAttribute("user") User user, BindingResult result, Model model) {
        try {
            if (result.hasErrors()) {
                LOGGER.debug("UserController (validate) -> Invalid fields provided");
                return "user/add";
            }
            userService.save(user);
            LOGGER.info("UserController (validate) -> User successfully added");
        } catch (UserAlreadyExistingException e) {
            model.addAttribute("userError", e.getMessage());
            return "user/add";
        }
        return "redirect:/user/list";
    }

    /**
     * Update user form.
     *
     * @param id    the user id to update
     * @param model the model
     * @return user update form view
     */
    @GetMapping("/user/update/{id}")
    public String showUpdateForm(@PathVariable("id") Integer id, Model model) {
        User user = userService.findById(id);
        LOGGER.info("UserController -> User to update found: " + user.toString());
        model.addAttribute("user", user);
        return "user/update";
    }

    /**
     * Update user.
     *
     * @param id     the user id to update
     * @param user   the user
     * @param result the result
     * @param model  the model
     * @return either update form view if BindingResult has error or redirect to user list view if is valid
     */
    @PostMapping("/user/update/{id}")
    public String updateUser(@PathVariable("id") Integer id, @Valid User user,
            BindingResult result, Model model) {
        try {
            if (result.hasErrors()) {
                LOGGER.debug("UserController (update) -> Invalid fields provided");
                return "user/update";
            }
            userService.update(user);
            LOGGER.info("UserController (update) -> User successfully updated: " + user.toString());
        } catch (UserAlreadyExistingException e) {
            model.addAttribute("userError", e.getMessage());
            return "user/update";
        }
        return "redirect:/user/list";
    }

    /**
     * Delete user.
     *
     * @param id the user id to delete
     * @return redirect to user list form view
     */
    @GetMapping("/user/delete/{id}")
    public String deleteUser(@PathVariable("id") Integer id) {
        LOGGER.info("UserController -> Deleting User with id:" + id);
        userService.deleteById(id);
        return "redirect:/user/list";
    }
}
