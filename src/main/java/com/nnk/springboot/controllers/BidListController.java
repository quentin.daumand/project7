package com.nnk.springboot.controllers;

import com.nnk.springboot.domain.BidList;
import com.nnk.springboot.services.BidListService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.List;


/**
 * The Bid list controller.
 */
@PreAuthorize("hasAnyRole('ADMIN','USER')")
@Controller
public class BidListController {

    /**
     * @see BidListService
     */
    @Autowired
    private BidListService bidListService;

    /**
     * @see Logger
     */
    private static final Logger LOGGER = LogManager.getLogger(
            BidListController.class);


    /**
     * Bid list home.
     *
     * @param model     the model
     * @param principal the OAuth2 user authenticated
     * @return bid list view
     */
    @RequestMapping("/bidList/list")
    public String home(Model model, @AuthenticationPrincipal OAuth2User principal) {
        List<BidList> bidListList = bidListService.findAll();
        model.addAttribute("bidlist", bidListService.findAll());
        if (principal != null) {
            model.addAttribute("login", principal.getAttribute("login"));
        }
        LOGGER.info("BidListController (home) -> " + bidListList.size() + " found");
        return "bidList/list";
    }

    /**
     * Add bid form.
     *
     * @param model the model
     * @return bid list add form view
     */
    @GetMapping("/bidList/add")
    public String addBidForm(Model model) {
        model.addAttribute("bidList", new BidList());
        LOGGER.info("BidListController (add) -> Accessing add form");

        return "bidList/add";
    }

    /**
     * Add bid list.
     *
     * @param bid    the bid
     * @param result the result
     * @return either add form view if BindingResult has error or redirect to bid list view if is valid
     */
    @PostMapping("/bidList/validate")
    public String validate(@Valid @ModelAttribute("bidList") BidList bid, BindingResult result) {
        if (result.hasErrors()) {
            LOGGER.debug("BidListController (validate) -> Invalid fields provided");
            return "bidList/add";
        }
        bidListService.save(bid);
        LOGGER.info("BidListController (validate) -> BidList successfully added");
        return "redirect:/bidList/list";
    }

    /**
     * Update Bid list form.
     *
     * @param id    the bid list id to update
     * @param model the model
     * @return bid list update form view
     */
    @GetMapping("/bidList/update/{id}")
    public String showUpdateForm(@PathVariable("id") Integer id, Model model) {
        BidList bidList = bidListService.findById(id);
        LOGGER.info("BidListController -> BidList to update found: " + bidList.toString());
        model.addAttribute("bidList", bidList);
        return "bidList/update";
    }

    /**
     * Update bid list.
     *
     * @param id      the bid list id to update
     * @param bidList the bid list
     * @param result  the result
     * @param model   the model
     * @return either update form view if BindingResult has error or redirect to bid list view if is valid
     */
    @PostMapping("/bidList/update/{id}")
    public String updateBid(@PathVariable("id") Integer id, @Valid BidList bidList,
            BindingResult result, Model model) {
        if (result.hasErrors()) {
            LOGGER.debug("BidListController (update) -> Invalid fields provided");
            return "bidList/update";
        }
        bidListService.save(bidList);
        LOGGER.info("BidListController (update) -> BidList successfully updated: " + bidList.toString());

        return "redirect:/bidList/list";
    }

    /**
     * Delete bid list.
     *
     * @param id the bid list id to delete
     * @return redirect to bid list form view
     */
    @GetMapping("/bidList/delete/{id}")
    public String deleteBid(@PathVariable("id") Integer id) {
        LOGGER.info("BidListController -> Deleting BidList with id:" + id);
        bidListService.deleteById(id);
        return "redirect:/bidList/list";
    }
}
