package com.nnk.springboot.controllers;

import com.nnk.springboot.domain.RuleName;
import com.nnk.springboot.services.RuleNameService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.List;

/**
 * The Rule name controller.
 */
@PreAuthorize("hasAnyRole('ADMIN','USER')")
@Controller
public class RuleNameController {

    /**
     * @see RuleNameService
     */
    @Autowired
    private RuleNameService ruleNameService;

    /**
     * @see Logger
     */
    private static final Logger LOGGER = LogManager.getLogger(
            RuleNameController.class);

    /**
     * Rule name home.
     *
     * @param model     the model
     * @param principal principal the OAuth2 user authenticated
     * @return redirect to rule name list view
     */
    @RequestMapping("/ruleName/list")
    public String home(Model model, @AuthenticationPrincipal OAuth2User principal) {
        List<RuleName> ruleNameList = ruleNameService.findAll();
        model.addAttribute("ruleNameList", ruleNameList);
        if (principal != null) {
            model.addAttribute("login", principal.getAttribute("login"));
        }
        LOGGER.info("RuleNameController (home) -> " + ruleNameList.size() + " found");
        return "ruleName/list";
    }

    /**
     * Add rule name form.
     *
     * @param model the model
     * @return rating add form view
     */
    @GetMapping("/ruleName/add")
    public String addRuleForm(Model model) {
        model.addAttribute("ruleName", new RuleName());
        LOGGER.info("RuleNameController (add) -> Accessing add form");
        return "ruleName/add";
    }

    /**
     * Add rule name.
     *
     * @param ruleName the rule name
     * @param result   the result
     * @return either add form view if BindingResult has error or redirect to rule name list view if is valid
     */
    @PostMapping("/ruleName/validate")
    public String validate(@Valid @ModelAttribute("ruleName") RuleName ruleName, BindingResult result) {
        if (result.hasErrors()) {
            LOGGER.debug("RuleNameController (validate) -> Invalid fields provided");
            return "ruleName/add";
        }
        ruleNameService.save(ruleName);
        LOGGER.info("RuleNameController (validate) -> RuleName successfully added");
        return "redirect:/ruleName/list";
    }

    /**
     * Update rule name form.
     *
     * @param id    the rule name id to update
     * @param model the model
     * @return rule name update form view
     */
    @GetMapping("/ruleName/update/{id}")
    public String showUpdateForm(@PathVariable("id") Integer id, Model model) {
        RuleName ruleName = ruleNameService.findById(id);
        LOGGER.info("RuleNameController -> RuleName to update found: " + ruleName.toString());
        model.addAttribute("ruleName", ruleName);
        return "ruleName/update";
    }

    /**
     * Update rule name.
     *
     * @param id       the rule name id to update
     * @param ruleName the rule name
     * @param result   the result
     * @param model    the model
     * @return either update form view if BindingResult has error or redirect to rule name list view if is valid
     */
    @PostMapping("/ruleName/update/{id}")
    public String updateRuleName(@PathVariable("id") Integer id, @Valid RuleName ruleName,
            BindingResult result, Model model) {
        if (result.hasErrors()) {
            LOGGER.debug("RuleNameController (update) -> Invalid fields provided");
            return "ruleName/update";
        }
        ruleNameService.save(ruleName);
        LOGGER.info("RuleNameController (update) -> RuleName successfully updated: " + ruleName.toString());
        return "redirect:/ruleName/list";
    }

    /**
     * Delete rule name.
     *
     * @param id the rule name id to delete
     * @return redirect to rating list form view
     */
    @GetMapping("/ruleName/delete/{id}")
    public String deleteRuleName(@PathVariable("id") Integer id) {
        LOGGER.info("RuleNameController -> Deleting RuleName with id:" + id);
        ruleNameService.deleteById(id);
        return "redirect:/ruleName/list";
    }
}
