package com.nnk.springboot.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * The type Home controller.
 */
@Controller
public class HomeController
{

    /**
     * @see Logger
     */
    private static final Logger LOGGER = LogManager.getLogger(
            HomeController.class);

    /**
     * Home.
     *
     * @return the home view
     */
    @RequestMapping("/")
    public String home() {
        LOGGER.info("HomeController -> Accessing index");
        return "home";
    }

    /**
     * Admin home.
     *
     * @return redirect to the bid list view
     */
    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping("/admin/home")
    public String adminHome() {
        LOGGER.info("HomeController -> Accessing admin page");
        return "redirect:/bidList/list";
    }


}
