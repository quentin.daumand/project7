package com.nnk.springboot.controllers;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * The type Login controller.
 */
@Controller
@RequestMapping("app")
public class LoginController {

    /**
     * @see Logger
     */
    private static final Logger LOGGER = LogManager.getLogger(
            LoginController.class);

    /**
     * Error.
     *
     * @param principal the OAuth2 user authenticated
     * @return the error 403 view
     */
    @GetMapping("error")
    public ModelAndView error(@AuthenticationPrincipal OAuth2User principal) {
        ModelAndView mav = new ModelAndView();
        String errorMessage = "You are not authorized for the requested data.";
        mav.addObject("errorMsg", errorMessage);
        if (principal != null) {
            mav.addObject("login", principal.getAttribute("login"));
        }
        mav.setViewName("403");
        LOGGER.debug("LoginController -> Redirecting to 403");
        return mav;
    }
}
