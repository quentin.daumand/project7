package com.nnk.springboot.controllers;

import com.nnk.springboot.domain.CurvePoint;
import com.nnk.springboot.services.CurvePointService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import javax.validation.Valid;
import java.util.List;

/**
 * The Curve controller.
 */
@PreAuthorize("hasAnyRole('ADMIN','USER')")
@Controller
public class CurveController {

    /**
     * @see CurvePointService
     */
    @Autowired
    private CurvePointService curvePointService;

    /**
     * @see Logger
     */
    private static final Logger LOGGER = LogManager.getLogger(
            CurveController.class);

    /**
     * Curve Point home.
     *
     * @param model     the model
     * @param principal he OAuth2 user authenticated
     * @return redirect to curve point list view
     */
    @RequestMapping("/curvePoint/list")
    public String home(Model model, @AuthenticationPrincipal OAuth2User principal) {
        List<CurvePoint> curvePointList = curvePointService.findAll();
        model.addAttribute("curvePointList", curvePointList);
        if (principal != null) {
            model.addAttribute("login", principal.getAttribute("login"));
        }
        LOGGER.info("CurveController (home) -> " + curvePointList.size() + " found");
        return "curvePoint/list";
    }

    /**
     * Add curve point
     *
     * @param model the model
     * @return curve point add form view
     */
    @GetMapping("/curvePoint/add")
    public String addCurvePointForm(Model model) {
        model.addAttribute("curvePoint", new CurvePoint());
        LOGGER.info("CurveController (add) -> Accessing add form");
        return "curvePoint/add";
    }

    /**
     * Add curve point.
     *
     * @param curvePoint the curve point
     * @param result     the result
     * @return either add form view if BindingResult has error or redirect to curve point list view if is valid
     */
    @PostMapping("/curvePoint/validate")
    public String validate(@Valid @ModelAttribute("curvePoint") CurvePoint curvePoint, BindingResult result) {
        if (result.hasErrors()) {
            LOGGER.debug("CurveController (validate) -> Invalid fields provided");
            return "curvePoint/add";
        }
        curvePointService.save(curvePoint);
        LOGGER.info("CurveController (validate) -> Curve Point successfully added");
        return "redirect:/curvePoint/list";
    }

    /**
     * Update Curve point form.
     *
     * @param id    the curve point id to update
     * @param model the model
     * @return curve point update form view
     */
    @GetMapping("/curvePoint/update/{id}")
    public String showUpdateForm(@PathVariable("id") Integer id, Model model) {
        CurvePoint curvePoint = curvePointService.findById(id);
        LOGGER.info("CurveController -> CurvePoint to update found: " + curvePoint.toString());
        model.addAttribute("curvePoint", curvePoint);
        return "curvePoint/update";
    }

    /**
     * Update curve point.
     *
     * @param id         the curve point id to update
     * @param curvePoint the curve point
     * @param result     the result
     * @param model      the model
     * @return either update form view if BindingResult has error or redirect to curve point list view if is valid
     */
    @PostMapping("/curvePoint/update/{id}")
    public String updateBid(@PathVariable("id") Integer id, @Valid CurvePoint curvePoint,
            BindingResult result, Model model) {
        if (result.hasErrors()) {
            LOGGER.debug("CurveController (update) -> Invalid fields provided");
            return "curvePoint/update";
        }
        curvePointService.save(curvePoint);
        LOGGER.info("CurveController (update) -> CurvePoint successfully updated: " + curvePoint.toString());
        return "redirect:/curvePoint/list";
    }

    /**
     * Delete curve point.
     *
     * @param id the curve point id to delete
     * @return redirect to curve point list form view
     */
    @GetMapping("/curvePoint/delete/{id}")
    public String deleteBid(@PathVariable("id") Integer id) {
        LOGGER.info("CurveController -> Deleting CurvePoint with id:" + id);
        curvePointService.deleteById(id);
        return "redirect:/curvePoint/list";
    }
}
