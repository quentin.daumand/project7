package com.nnk.springboot.controllers;

import com.nnk.springboot.domain.Rating;
import com.nnk.springboot.services.RatingService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.util.List;

/**
 * The Rating controller.
 */
@PreAuthorize("hasAnyRole('ADMIN','USER')")
@Controller
public class RatingController {

    /**
     * @see RatingService
     */
    @Autowired
    private RatingService ratingService;

    /**
     * @see Logger
     */
    private static final Logger LOGGER = LogManager.getLogger(
            RatingController.class);

    /**
     * Rating list home.
     *
     * @param model     the model
     * @param principal the OAuth2 user authenticated
     * @return redirect to rating list view
     */
    @RequestMapping("/rating/list")
    public String home(Model model, @AuthenticationPrincipal OAuth2User principal) {
        List<Rating> ratingList = ratingService.findAll();
        model.addAttribute("ratingList", ratingList);
        if (principal != null) {
            model.addAttribute("login", principal.getAttribute("login"));
        }
        LOGGER.info("RatingController (home) -> " + ratingList.size() + " found");
        return "rating/list";
    }

    /**
     * Add rating form.
     *
     * @param model the model
     * @return rating add form view
     */
    @GetMapping("/rating/add")
    public String addRatingForm(Model model) {
        model.addAttribute("rating", new Rating());
        LOGGER.info("RatingController (add) -> Accessing add form");
        return "rating/add";
    }

    /**
     * Add rating.
     *
     * @param rating the rating
     * @param result the result
     * @return either add form view if BindingResult has error or redirect to rating list view if is valid
     */
    @PostMapping("/rating/validate")
    public String validate(@Valid @ModelAttribute("rating") Rating rating, BindingResult result) {
        if (result.hasErrors()) {
            LOGGER.debug("RatingController (validate) -> Invalid fields provided");
            return "rating/add";
        }
        ratingService.save(rating);
        LOGGER.info("RatingController (validate) -> Rating successfully added");
        return "redirect:/rating/list";
    }

    /**
     * Update rating form.
     *
     * @param id    the rating id to update
     * @param model the model
     * @return rating update form view
     */
    @GetMapping("/rating/update/{id}")
    public String showUpdateForm(@PathVariable("id") Integer id, Model model) {
        Rating rating = ratingService.findById(id);
        LOGGER.info("RatingController -> Rating to update found: " + rating.toString());
        model.addAttribute("rating", rating);
        return "rating/update";
    }

    /**
     * Update rating.
     *
     * @param id     the rating id to update
     * @param rating the rating
     * @param result the result
     * @param model  the model
     * @return either update form view if BindingResult has error or redirect to rating view if is valid
     */
    @PostMapping("/rating/update/{id}")
    public String updateRating(@PathVariable("id") Integer id, @Valid Rating rating,
            BindingResult result, Model model) {
        if (result.hasErrors()) {
            LOGGER.debug("RatingController (update) -> Invalid fields provided");
            return "rating/update";
        }
        ratingService.save(rating);
        LOGGER.info("RatingController (update) -> Rating successfully updated: " + rating.toString());
        return "redirect:/rating/list";
    }

    /**
     * Delete rating.
     *
     * @param id the rating id to delete
     * @return redirect to rating list form view
     */
    @GetMapping("/rating/delete/{id}")
    public String deleteRating(@PathVariable("id") Integer id) {
        LOGGER.info("RatingController -> Deleting Rating with id:" + id);
        ratingService.deleteById(id);
        return "redirect:/rating/list";
    }
}
