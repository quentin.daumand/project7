package com.nnk.springboot.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;


/**
 * The type Curve point.
 */
@ToString
@Getter
@Setter
@Entity
@Table(name = "curve_point")
public class CurvePoint {

    /**
     * Instantiates a new Curve point.
     */
    public CurvePoint() {
    }

    /**
     * Instantiates a new Curve point.
     *
     * @param curveId the curve id
     * @param term    the term
     * @param value   the value
     */
    public CurvePoint(Integer curveId, Double term, Double value) {
        this.curveId = curveId;
        this.term = term;
        this.value = value;
    }

    /**
     * The Id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * The Curve id.
     */
    @NotNull(message = "Curve id is mandatory")
    @Min(value = 0, message = "Number must be positive")
    @Digits(integer = 8, fraction = 0, message = "Should be a number")
    private Integer curveId;

    /**
     * The As of date.
     */
    @Column(name = "as_of_date")
    private LocalDateTime asOfDate;

    /**
     * The Term.
     */
    @NotNull(message = "Term is mandatory")
    @Min(value = 0, message = "Number must be positive")
    @Digits(integer = 8, fraction = 2, message = "Should be a number")
    private Double term;

    /**
     * The Value.
     */
    @NotNull(message = "Value is mandatory")
    @Min(value = 0, message = "Number must be positive")
    @Digits(integer = 8, fraction = 2, message = "Should be a number")
    private Double value;

    /**
     * The Creation date.
     */
    @Column(name = "creation_date")
    private LocalDateTime creationDate;

}
