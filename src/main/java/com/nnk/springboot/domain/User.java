package com.nnk.springboot.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.CredentialsContainer;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * The type User.
 */
@Getter
@Setter
@Entity
@Table(name = "user")
public class User implements UserDetails, CredentialsContainer {

    /**
     * Instantiates a new User.
     */
    public User() {
    }

    /**
     * Instantiates a new User.
     *
     * @param username the username
     * @param fullName the full name
     * @param role     the role
     */
    public User(String username, String fullName, String role) {
        this.username = username;
        this.fullName = fullName;
        this.role = role;
    }

    /**
     * The Id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * The Username.
     */
    @NotBlank(message = "Username is mandatory")
    private String username;

    /**
     * The Password.
     */

             
    @NotBlank(message = "Password is mandatory")
    private String password;

    /**
     * The Full name.
     */
    @Column(name = "full_name")
    @NotBlank(message = "FullName is mandatory")
    private String fullName;

    /**
     * The Role.
     */
    @NotBlank(message = "Role is mandatory")
    private String role;

    @Override
    public void eraseCredentials() {
    }

    @Override
    public String getUsername() {
        return this.username;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> grantedAuthorityList = new ArrayList<>();
        grantedAuthorityList.add(new SimpleGrantedAuthority(this.role));
        return grantedAuthorityList;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
