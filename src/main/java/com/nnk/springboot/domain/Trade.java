package com.nnk.springboot.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.time.LocalDateTime;


/**
 * The type Trade.
 */
@ToString
@Getter
@Setter
@Entity
@Table(name = "trade")
public class Trade {

    /**
     * Instantiates a new Trade.
     */
    public Trade() {
    }

    /**
     * Instantiates a new Trade.
     *
     * @param account     the account
     * @param type        the type
     * @param buyQuantity the buy quantity
     */
    public Trade(String account, String type, Double buyQuantity) {
        this.account = account;
        this.type = type;
        this.buyQuantity = buyQuantity;
    }

    /**
     * The Trade id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "trade_id")
    private Integer tradeId;

    /**
     * The Account.
     */
    @NotEmpty(message = "Account is mandatory")
    private String account;

    /**
     * The Type.
     */
    @NotEmpty(message = "Type is mandatory")
    private String type;

    /**
     * The Buy quantity.
     */
    @NotNull(message = "Buy quantity is mandatory")
    @Min(value = 0, message = "Number must be positive")
    @Digits(integer = 8, fraction = 2, message = "Should be a number")
    @Column(name = "buy_quantity")
    private Double buyQuantity;

    /**
     * The Sell quantity.
     */
    @Column(name = "sell_quantity")
    private Double sellQuantity;

    /**
     * The Buy price.
     */
    @Column(name = "buy_price")
    private Double buyPrice;

    /**
     * The Sell price.
     */
    @Column(name = "sell_price")
    private Double sellPrice;

    /**
     * The Trade date.
     */
    @Column(name = "trade_date")
    private LocalDateTime tradeDate;

    /**
     * The Security.
     */
    private String security;

    /**
     * The Status.
     */
    private String status;

    /**
     * The Trader.
     */
    private String trader;

    /**
     * The Benchmark.
     */
    private String benchmark;

    /**
     * The Book.
     */
    private String book;

    /**
     * The Creation name.
     */
    @Column(name = "creation_name")
    private String creationName;

    /**
     * The Creation date.
     */
    @Column(name = "creation_date")
    private LocalDateTime creationDate;

    /**
     * The Revision name.
     */
    @Column(name = "revision_name")
    private String revisionName;

    /**
     * The Revision date.
     */
    @Column(name = "revision_date")
    private LocalDateTime revisionDate;

    /**
     * The Deal name.
     */
    @Column(name = "deal_name")
    private String dealName;

    /**
     * The Deal type.
     */
    @Column(name = "deal_type")
    private String dealType;

    /**
     * The Source list id.
     */
    @Column(name = "source_list_id")
    private String sourceListId;

    /**
     * The Side.
     */
    private String side;

}
