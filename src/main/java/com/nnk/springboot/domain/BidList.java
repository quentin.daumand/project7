package com.nnk.springboot.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * The type Bid list.
 */
@ToString
@Getter
@Setter
@Entity
@Table(name = "bid_list")
public class BidList {

    /**
     * Instantiates a new Bid list.
     */
    public BidList() {
    }

    /**
     * Instantiates a new Bid list.
     *
     * @param account     the account
     * @param type        the type
     * @param bidQuantity the bid quantity
     */
    public BidList(String account, String type, Double bidQuantity) {
        this.account = account;
        this.type = type;
        this.bidQuantity = bidQuantity;
    }

    /**
     * The Bid list id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "bid_list_id")
    private Integer bidListId;

    /**
     * The Account.
     */
    @NotEmpty(message = "Account is mandatory")
    private String account;

    /**
     * The Type.
     */
    @NotEmpty(message = "Type is mandatory")
    private String type;

    /**
     * The Bid quantity.
     */
    @NotNull(message = "Bid quantity is mandatory")
    @Min(value = 0, message = "Number must be positive")
    @Digits(integer = 8, fraction = 2, message = "Should be a number")
    @Column(name = "bid_quantity")
    private Double bidQuantity;

    /**
     * The Ask quantity.
     */
    @Column(name = "ask_quantity")
    private Double askQuantity;

    /**
     * The Bid.
     */
    private Double bid;

    /**
     * The Ask.
     */
    private Double ask;

    /**
     * The Benchmark.
     */
    private String benchmark;

    /**
     * The Bid list date.
     */
    @Column(name = "bid_list_date")
    private LocalDateTime bidListDate;

    /**
     * The Commentary.
     */
    private String commentary;

    /**
     * The Security.
     */
    private String security;

    /**
     * The Status.
     */
    private String status;

    /**
     * The Trader.
     */
    private String trader;

    /**
     * The Book.
     */
    private String book;

    /**
     * The Creation name.
     */
    @Column(name = "creation_name")
    private String creationName;

    /**
     * The Creation date.
     */
    @Column(name = "creation_date")
    private LocalDateTime creationDate;

    /**
     * The Revision name.
     */
    @Column(name = "revision_name")
    private String revisionName;

    /**
     * The Revision date.
     */
    @Column(name = "revision_date")
    private LocalDateTime revisionDate;

    /**
     * The Deal name.
     */
    @Column(name = "deal_name")
    private String dealName;

    /**
     * The Deal type.
     */
    @Column(name = "deal_type")
    private String dealType;

    /**
     * The Source list id.
     */
    @Column(name = "source_list_id")
    private String sourceListId;

    /**
     * The Side.
     */
    private String side;

}
