package com.nnk.springboot.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.sql.Timestamp;

/**
 * The type Rule name.
 */
@ToString
@Getter
@Setter
@Entity
@Table(name = "rule_name")
public class RuleName {

    /**
     * Instantiates a new Rule name.
     */
    public RuleName() {
    }

    /**
     * Instantiates a new Rule name.
     *
     * @param name        the name
     * @param description the description
     * @param json        the json
     * @param template    the template
     * @param sqlStr      the sql str
     * @param sqlPart     the sql part
     */
    public RuleName(String name, String description, String json, String template, String sqlStr, String sqlPart) {
        this.name = name;
        this.description = description;
        this.json = json;
        this.template = template;
        this.sqlStr = sqlStr;
        this.sqlPart = sqlPart;
    }

    /**
     * The Id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * The Name.
     */
    @NotEmpty(message = "Name is mandatory")
    private String name;

    /**
     * The Description.
     */
    @NotEmpty(message = "Description is mandatory")
    private String description;

    /**
     * The Json.
     */
    @NotEmpty(message = "Json is mandatory")
    private String json;

    /**
     * The Template.
     */
    @NotEmpty(message = "Template is mandatory")
    private String template;

    /**
     * The Sql str.
     */
    @NotEmpty(message = "Sql str is mandatory")
    @Column(name = "sql_str")
    private String sqlStr;

    /**
     * The Sql part.
     */
    @NotEmpty(message = "Sql part is mandatory")
    @Column(name = "sql_part")
    private String sqlPart;

}
