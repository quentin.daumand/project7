package com.nnk.springboot.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * The type Rating.
 */
@ToString
@Getter
@Setter
@Entity
@Table(name = "rating")
public class Rating {

    /**
     * Instantiates a new Rating.
     */
    public Rating() {
    }

    /**
     * Instantiates a new Rating.
     *
     * @param moodysRating the moodys rating
     * @param sandPRating  the sand p rating
     * @param fitchRating  the fitch rating
     * @param orderNumber  the order number
     */
    public Rating(String moodysRating, String sandPRating, String fitchRating, Integer orderNumber) {
        this.moodysRating = moodysRating;
        this.sandPRating = sandPRating;
        this.fitchRating = fitchRating;
        this.orderNumber = orderNumber;
    }

    /**
     * The Id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * The Moodys rating.
     */
    @NotEmpty(message = "Moodys rating is mandatory")
    @Column(name = "moodys_rating")
    private String moodysRating;

    /**
     * The Sand p rating.
     */
    @NotEmpty(message = "Sand P rating is mandatory")
    @Column(name = "sand_p_rating")
    private String sandPRating;

    /**
     * The Fitch rating.
     */
    @NotEmpty(message = "Fitch rating is mandatory")
    @Column(name = "fitch_rating")
    private String fitchRating;

    /**
     * The Order number.
     */
    @NotNull(message = "Order number is mandatory")
    @Min(value = 0, message = "Number must be positive")
    @Digits(integer = 8, fraction = 0, message = "Should be a number")
    @Column(name = "order_number")
    private Integer orderNumber;

}
