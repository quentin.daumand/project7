CREATE TABLE bid_list
(
    bid_list_id    tinyint(4)  NOT NULL AUTO_INCREMENT,
    account        VARCHAR(30) NOT NULL,
    type           VARCHAR(30) NOT NULL,
    bid_quantity   DOUBLE,
    ask_quantity   DOUBLE,
    bid            DOUBLE,
    ask            DOUBLE,
    benchmark      VARCHAR(125),
    bid_list_date  TIMESTAMP,
    commentary     VARCHAR(125),
    security       VARCHAR(125),
    status         VARCHAR(10),
    trader         VARCHAR(125),
    book           VARCHAR(125),
    creation_name  VARCHAR(125),
    creation_date  TIMESTAMP,
    revision_name  VARCHAR(125),
    revision_date  TIMESTAMP,
    deal_name      VARCHAR(125),
    deal_type      VARCHAR(125),
    source_list_id VARCHAR(125),
    side           VARCHAR(125),

    PRIMARY KEY (bid_list_id)
);

CREATE TABLE trade
(
    trade_id       tinyint(4)  NOT NULL AUTO_INCREMENT,
    account        VARCHAR(30) NOT NULL,
    type           VARCHAR(30) NOT NULL,
    buy_quantity   DOUBLE,
    sell_quantity  DOUBLE,
    buy_price      DOUBLE,
    sell_price     DOUBLE,
    trade_date     TIMESTAMP,
    security       VARCHAR(125),
    status         VARCHAR(10),
    trader         VARCHAR(125),
    benchmark      VARCHAR(125),
    book           VARCHAR(125),
    creation_name  VARCHAR(125),
    creation_date  TIMESTAMP,
    revision_name  VARCHAR(125),
    revision_date  TIMESTAMP,
    deal_name      VARCHAR(125),
    deal_type      VARCHAR(125),
    source_list_id VARCHAR(125),
    side           VARCHAR(125),

    PRIMARY KEY (trade_id)
);

CREATE TABLE curve_point
(
    Id            tinyint(4) NOT NULL AUTO_INCREMENT,
    curve_id      tinyint,
    as_of_date    TIMESTAMP,
    term          DOUBLE,
    value         DOUBLE,
    creation_date TIMESTAMP,

    PRIMARY KEY (Id)
);

CREATE TABLE rating
(
    Id            tinyint(4) NOT NULL AUTO_INCREMENT,
    moodys_rating VARCHAR(125),
    sand_p_rating VARCHAR(125),
    fitch_rating  VARCHAR(125),
    order_number  tinyint,

    PRIMARY KEY (Id)
);

CREATE TABLE rule_name
(
    Id          tinyint(4) NOT NULL AUTO_INCREMENT,
    name        VARCHAR(125),
    description VARCHAR(125),
    json        VARCHAR(125),
    template    VARCHAR(512),
    sql_str     VARCHAR(125),
    sql_part    VARCHAR(125),

    PRIMARY KEY (Id)
);

CREATE TABLE user
(
    Id        tinyint(4) NOT NULL AUTO_INCREMENT,
    username  VARCHAR(125),
    password  VARCHAR(125),
    full_name VARCHAR(125),
    role      VARCHAR(125),

    PRIMARY KEY (Id)
);

insert into User(full_name, username, password, role)
values ("Administrator", "admin", "$2y$10$bOvXintMRHmlSjazI.8I7eKmQFE0vzksowz6rR.SRcGO1fhiPa4Du", "ROLE_ADMIN");
insert into User(full_name, username, password, role)
values ("User", "user", "$2y$10$UCmfYNuc6CblB6uk6VfJieJJJM0zdCou2ah49gIBRPCS9Ra64nJLC", "ROLE_USER");
insert into bid_list (account, type, bid_quantity)
values ("Test", "Type test", 10.10);
insert into bid_list (account, type, bid_quantity)
values ("Test", "Type test", 20.10);
insert into bid_list (account, type, bid_quantity)
values ("Test", "Type test", 30.10);
insert into curve_point (curve_id, term, value)
values (1, 10.00, 20.00);
insert into curve_point (curve_id, term, value)
values (1, 20.00, 30.00);
insert into rating (moodys_rating, sand_p_rating, fitch_rating, order_number)
values ("Test", "Test", "Test", 10);
insert into rating (moodys_rating, sand_p_rating, fitch_rating, order_number)
values ("Test2", "Test2", "Test2", 20);
insert into rule_name (name, description, json, template, sql_str, sql_part)
values ("Test", "Test", "Test", "Test", "Test", "Test");
insert into rule_name (name, description, json, template, sql_str, sql_part)
values ("Test2", "Test2", "Test2", "Test2", "Test2", "Test2");
insert into trade (account, type, buy_quantity)
values ("Test", "Test", 10.00);
insert into trade (account, type, buy_quantity)
values ("Test2", "Test2", 20.00);