package com.nnk.springboot.controllers;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(LoginController.class)
public class LoginControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    @WithAnonymousUser
    void should_Return_Unauthorized_No_Auth() throws Exception {
        mockMvc.perform(get("/app/error"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    void should_Return_Error_Page_User_Auth() throws Exception {
        mockMvc.perform(get("/app/error"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void should_Return_Error_Page_Admin_Auth() throws Exception {
        mockMvc.perform(get("/app/error"))
                .andExpect(status().isOk());
    }

}
