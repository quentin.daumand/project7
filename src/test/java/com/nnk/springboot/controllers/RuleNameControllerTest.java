package com.nnk.springboot.controllers;

import com.nnk.springboot.domain.BidList;
import com.nnk.springboot.domain.RuleName;
import com.nnk.springboot.services.RuleNameService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import java.util.ArrayList;
import java.util.List;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(RuleNameController.class)
public class RuleNameControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RuleNameService ruleNameService;

    private static List<RuleName> ruleNameList;

    @BeforeEach
    void setUpPerTest() {
        ruleNameList = new ArrayList<>();
        ruleNameList.add(new RuleName("test", "test", "test", "test", "test", "test"));
        ruleNameList.add(new RuleName("test", "test", "test", "test", "test", "test"));
    }

    @Test
    @WithAnonymousUser
    void should_Return_Unauthorized_No_Auth() throws Exception {
        mockMvc.perform(get("/ruleName/list"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    void should_Return_RuleName_List_User_Auth() throws Exception {
        when(ruleNameService.findAll()).thenReturn(ruleNameList);
        mockMvc.perform(get("/ruleName/list"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("ruleNameList"));
    }

    @Test
    @WithAnonymousUser
    void should_Return_RuleName_Form_No_Auth() throws Exception {
        mockMvc.perform(get("/ruleName/add"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    void should_Return_RuleName_Form_User_Auth() throws Exception {
        mockMvc.perform(get("/ruleName/add"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("ruleName"));
    }

    @Test
    @WithAnonymousUser
    void addRuleNameNoAuth() throws Exception {
        RuleName ruleName = new RuleName("test", "test", "test", "test", "test", "test");
        mockMvc.perform(post("/ruleName/validate").flashAttr("ruleName", ruleName))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    void addRuleNameWithErrorInFieldUserAuth() throws Exception {
        RuleName ruleName = new RuleName("", "test", "test", "test", "test", "test");
        mockMvc.perform(post("/ruleName/validate").flashAttr("ruleName", ruleName))
                .andExpect(status().isOk())
                .andExpect(model().hasErrors());
    }

    @Test
    @WithMockUser
    void addRuleNameWithNoErrorInField() throws Exception {
        RuleName ruleName = new RuleName("test", "test", "test", "test", "test", "test");
        mockMvc.perform(post("/ruleName/validate")
                                .flashAttr("ruleName",
                                           ruleName))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @WithAnonymousUser
    void showUpdateFormNoAuth() throws Exception {
        RuleName ruleName = new RuleName("test", "test", "test", "test", "test", "test");
        when(ruleNameService.findById(anyInt())).thenReturn(ruleName);
        mockMvc.perform(get("/ruleName/update/1").flashAttr("ruleName", ruleName))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    void showUpdateFormUserAuth() throws Exception {
        RuleName ruleName = new RuleName("test", "test", "test", "test", "test", "test");
        when(ruleNameService.findById(anyInt())).thenReturn(ruleName);
        mockMvc.perform(get("/ruleName/update/1").flashAttr("ruleName", ruleName))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("ruleName"));
    }

    @Test
    @WithAnonymousUser
    void saveUpdateNoAuth() throws Exception {
        RuleName ruleName = new RuleName("test", "test", "test", "test", "test", "test");
        when(ruleNameService.findById(anyInt())).thenReturn(ruleName);
        mockMvc.perform(post("/ruleName/update/1").flashAttr("ruleName", ruleName))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    void saveUpdateWithErrorInFieldsUserAuth() throws Exception {
        RuleName ruleName = new RuleName("", "test", "test", "test", "test", "test");
        when(ruleNameService.findById(anyInt())).thenReturn(ruleName);
        mockMvc.perform(post("/ruleName/update/1").flashAttr("ruleName", ruleName))
                .andExpect(status().isOk())
                .andExpect(model().hasErrors());
    }

    @Test
    @WithMockUser
    void saveUpdateWithNoErrorInFieldsUserAuth() throws Exception {
        RuleName ruleName = new RuleName("test", "test", "test", "test", "test", "test");
        when(ruleNameService.findById(anyInt())).thenReturn(ruleName);
        mockMvc.perform(post("/ruleName/update/1").flashAttr("ruleName", ruleName))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @WithAnonymousUser
    void deleteNoAuth() throws Exception {
        mockMvc.perform(get("/ruleName/delete/1"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    void deleteUserAuth() throws Exception {
        mockMvc.perform(get("/ruleName/delete/1"))
                .andExpect(status().is3xxRedirection());
    }

}
