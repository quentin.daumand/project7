package com.nnk.springboot.controllers;

import com.nnk.springboot.domain.BidList;
import com.nnk.springboot.services.BidListService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import java.util.ArrayList;
import java.util.List;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(BidListController.class)
public class BidListControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BidListService bidListService;

    private static List<BidList> bidListList;

    @BeforeEach
    void setUpPerTest() {
        bidListList = new ArrayList<>();
        bidListList.add(new BidList("Test account", "Test type", 10.00d));
        bidListList.add(new BidList("Test account2", "Test type2", 20.00d));
    }

    @Test
    @WithAnonymousUser
    void should_Return_Unauthorized_No_Auth() throws Exception {
        mockMvc.perform(get("/bidList/list"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    void should_Return_Bid_List_User_Auth() throws Exception {
        when(bidListService.findAll()).thenReturn(bidListList);
        mockMvc.perform(get("/bidList/list"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("bidlist"));
    }

    @Test
    @WithAnonymousUser
    void should_Return_Bid_Form_No_Auth() throws Exception {
        mockMvc.perform(get("/bidList/add"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    void should_Return_Bid_Form_User_Auth() throws Exception {
        mockMvc.perform(get("/bidList/add"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("bidList"));
    }

    @Test
    @WithAnonymousUser
    void addBidListNoAuth() throws Exception {
        BidList bidList = new BidList(null, "test", 123.23);
        mockMvc.perform(post("/bidList/validate").flashAttr("bidList", bidList))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    void addBidListWithErrorInFieldUserAuth() throws Exception {
        BidList bidList = new BidList(null, "test", 123.23);
        mockMvc.perform(post("/bidList/validate").flashAttr("bidList", bidList))
                .andExpect(status().isOk())
                .andExpect(model().hasErrors());
    }

    @Test
    @WithMockUser
    void addBidListWithNoErrorInField() throws Exception {
        BidList bidList = new BidList("test", "test", 123.23);
        mockMvc.perform(post("/bidList/validate")
                                .flashAttr("bidList",
                                           bidList))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @WithAnonymousUser
    void showUpdateFormNoAuth() throws Exception {
        BidList bidList = new BidList("test", "test", 123.23);
        when(bidListService.findById(anyInt())).thenReturn(bidList);
        mockMvc.perform(get("/bidList/update/1").flashAttr("bidList", bidList))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    void showUpdateFormUserAuth() throws Exception {
        BidList bidList = new BidList("test", "test", 123.23);
        when(bidListService.findById(anyInt())).thenReturn(bidList);
        mockMvc.perform(get("/bidList/update/1").flashAttr("bidList", bidList))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("bidList"));
    }

    @Test
    @WithAnonymousUser
    void saveUpdateNoAuth() throws Exception {
        BidList bidList = new BidList("test", "test", 123.23);
        when(bidListService.findById(anyInt())).thenReturn(bidList);
        mockMvc.perform(post("/bidList/update/1").flashAttr("bidList", bidList))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    void saveUpdateWithErrorInFieldsUserAuth() throws Exception {
        BidList bidList = new BidList(null, "test", 123.23);
        when(bidListService.findById(anyInt())).thenReturn(bidList);
        mockMvc.perform(post("/bidList/update/1").flashAttr("bidList", bidList))
                .andExpect(status().isOk())
                .andExpect(model().hasErrors());
    }

    @Test
    @WithMockUser
    void saveUpdateWithNoErrorInFieldsUserAuth() throws Exception {
        BidList bidList = new BidList("test", "test", 123.23);
        when(bidListService.findById(anyInt())).thenReturn(bidList);
        mockMvc.perform(post("/bidList/update/1").flashAttr("bidList", bidList))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @WithAnonymousUser
    void deleteNoAuth() throws Exception {
        mockMvc.perform(get("/bidList/delete/1"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    void deleteUserAuth() throws Exception {
        mockMvc.perform(get("/bidList/delete/1"))
                .andExpect(status().is3xxRedirection());
    }


}
