package com.nnk.springboot.controllers;

import com.nnk.springboot.domain.BidList;
import com.nnk.springboot.domain.Trade;
import com.nnk.springboot.services.TradeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import java.util.ArrayList;
import java.util.List;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(TradeController.class)
public class TradeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TradeService tradeService;

    private static List<Trade> tradeList;

    @BeforeEach
    void setUpPerTest() {
        tradeList = new ArrayList<>();
        tradeList.add(new Trade("test", "test", 20.00d));
        tradeList.add(new Trade("test", "test", 30.00d));
    }

    @Test
    @WithAnonymousUser
    void should_Return_Unauthorized_No_Auth() throws Exception {
        mockMvc.perform(get("/trade/list"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    void should_Return_Trade_List_User_Auth() throws Exception {
        when(tradeService.findAll()).thenReturn(tradeList);
        mockMvc.perform(get("/trade/list"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("tradeList"));
    }

    @Test
    @WithAnonymousUser
    void should_Return_Trade_Form_No_Auth() throws Exception {
        mockMvc.perform(get("/trade/add"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    void should_Return_Trade_Form_User_Auth() throws Exception {
        mockMvc.perform(get("/trade/add"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("trade"));
    }

    @Test
    @WithAnonymousUser
    void addBidListNoAuth() throws Exception {
        Trade trade = new Trade("test", "test", 20.00d);
        mockMvc.perform(post("/trade/validate").flashAttr("trade", trade))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    void addTradeWithErrorInFieldUserAuth() throws Exception {
        Trade trade = new Trade("test", "test", null);
        mockMvc.perform(post("/trade/validate").flashAttr("trade", trade))
                .andExpect(status().isOk())
                .andExpect(model().hasErrors());
    }

    @Test
    @WithMockUser
    void addTradeWithNoErrorInField() throws Exception {
        Trade trade = new Trade("test", "test", 20.00d);
        mockMvc.perform(post("/trade/validate")
                                .flashAttr("trade",
                                           trade))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @WithAnonymousUser
    void showUpdateFormNoAuth() throws Exception {
        Trade trade = new Trade("test", "test", 20.00d);
        when(tradeService.findById(anyInt())).thenReturn(trade);
        mockMvc.perform(get("/trade/update/1").flashAttr("trade", trade))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    void showUpdateFormUserAuth() throws Exception {
        Trade trade = new Trade("test", "test", 20.00d);
        when(tradeService.findById(anyInt())).thenReturn(trade);
        mockMvc.perform(get("/trade/update/1").flashAttr("trade", trade))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("trade"));
    }

    @Test
    @WithAnonymousUser
    void saveUpdateNoAuth() throws Exception {
        Trade trade = new Trade("test", "test", 20.00d);
        when(tradeService.findById(anyInt())).thenReturn(trade);
        mockMvc.perform(post("/trade/update/1").flashAttr("trade", trade))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    void saveUpdateWithErrorInFieldsUserAuth() throws Exception {
        Trade trade = new Trade("test", "test", -1.00);
        when(tradeService.findById(anyInt())).thenReturn(trade);
        mockMvc.perform(post("/trade/update/1").flashAttr("trade", trade))
                .andExpect(status().isOk())
                .andExpect(model().hasErrors());
    }

    @Test
    @WithMockUser
    void saveUpdateWithNoErrorInFieldsUserAuth() throws Exception {
        Trade trade = new Trade("test", "test", 20.00d);
        when(tradeService.findById(anyInt())).thenReturn(trade);
        mockMvc.perform(post("/trade/update/1").flashAttr("trade", trade))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @WithAnonymousUser
    void deleteNoAuth() throws Exception {
        mockMvc.perform(get("/trade/delete/1"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    void deleteUserAuth() throws Exception {
        mockMvc.perform(get("/trade/delete/1"))
                .andExpect(status().is3xxRedirection());
    }

}
