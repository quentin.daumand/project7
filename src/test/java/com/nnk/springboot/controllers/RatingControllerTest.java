package com.nnk.springboot.controllers;

import com.nnk.springboot.domain.BidList;
import com.nnk.springboot.domain.Rating;
import com.nnk.springboot.services.RatingService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import java.util.ArrayList;
import java.util.List;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(RatingController.class)
public class RatingControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RatingService ratingService;

    private static List<Rating> ratingList;

    @BeforeEach
    void setUpPerTest() {
        ratingList = new ArrayList<>();
        ratingList.add(new Rating("test", "test", "test", 1));
        ratingList.add(new Rating("test", "test", "test", 1));
    }

    @Test
    @WithAnonymousUser
    void should_Return_Unauthorized_No_Auth() throws Exception {
        mockMvc.perform(get("/rating/list"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    void should_Return_Rating_List_User_Auth() throws Exception {
        when(ratingService.findAll()).thenReturn(ratingList);
        mockMvc.perform(get("/rating/list"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("ratingList"));
    }

    @Test
    @WithAnonymousUser
    void should_Return_Rating_Form_No_Auth() throws Exception {
        mockMvc.perform(get("/rating/add"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    void should_Return_Rating_Form_User_Auth() throws Exception {
        mockMvc.perform(get("/rating/add"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("rating"));
    }

    @Test
    @WithAnonymousUser
    void addRatingNoAuth() throws Exception {
        Rating rating = new Rating("test", "test", "test", 1);
        mockMvc.perform(post("/rating/validate").flashAttr("rating", rating))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    void addRatingWithErrorInFieldUserAuth() throws Exception {
        Rating rating = new Rating("test", "test", "test", null);
        mockMvc.perform(post("/rating/validate").flashAttr("rating", rating))
                .andExpect(status().isOk())
                .andExpect(model().hasErrors());
    }

    @Test
    @WithMockUser
    void addBidListWithNoErrorInField() throws Exception {
        Rating rating = new Rating("test", "test", "test", 1);
        mockMvc.perform(post("/rating/validate")
                                .flashAttr("rating",
                                           rating))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @WithAnonymousUser
    void showUpdateFormNoAuth() throws Exception {
        Rating rating = new Rating("test", "test", "test", 1);
        when(ratingService.findById(anyInt())).thenReturn(rating);
        mockMvc.perform(get("/rating/update/1").flashAttr("rating", rating))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    void showUpdateFormUserAuth() throws Exception {
        Rating rating = new Rating("test", "test", "test", 1);
        when(ratingService.findById(anyInt())).thenReturn(rating);
        mockMvc.perform(get("/rating/update/1").flashAttr("rating", rating))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("rating"));
    }

    @Test
    @WithAnonymousUser
    void saveUpdateNoAuth() throws Exception {
        Rating rating = new Rating("test", "test", "test", 1);
        when(ratingService.findById(anyInt())).thenReturn(rating);
        mockMvc.perform(post("/rating/update/1").flashAttr("rating", rating))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    void saveUpdateWithErrorInFieldsUserAuth() throws Exception {
        Rating rating = new Rating("test", "test", "test", null);
        when(ratingService.findById(anyInt())).thenReturn(rating);
        mockMvc.perform(post("/rating/update/1").flashAttr("rating", rating))
                .andExpect(status().isOk())
                .andExpect(model().hasErrors());
    }

    @Test
    @WithMockUser
    void saveUpdateWithNoErrorInFieldsUserAuth() throws Exception {
        Rating rating = new Rating("test", "test", "test", 1);
        when(ratingService.findById(anyInt())).thenReturn(rating);
        mockMvc.perform(post("/rating/update/1").flashAttr("rating", rating))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @WithAnonymousUser
    void deleteNoAuth() throws Exception {
        mockMvc.perform(get("/rating/delete/1"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    void deleteUserAuth() throws Exception {
        mockMvc.perform(get("/rating/delete/1"))
                .andExpect(status().is3xxRedirection());
    }

}
