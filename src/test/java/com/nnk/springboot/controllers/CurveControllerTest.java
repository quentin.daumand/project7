package com.nnk.springboot.controllers;

import com.nnk.springboot.domain.CurvePoint;
import com.nnk.springboot.services.CurvePointService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import java.util.ArrayList;
import java.util.List;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CurveController.class)
public class CurveControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CurvePointService curveService;

    private static List<CurvePoint> curvePointList;


    @BeforeEach
    void setUpPerTest() {
        curvePointList = new ArrayList<>();
        curvePointList.add(new CurvePoint(1,14d,10.0d));
        curvePointList.add(new CurvePoint(2,16d,12.0d));
    }

    @Test
    @WithAnonymousUser
    void should_Return_Unauthorized_No_Auth() throws Exception {
        mockMvc.perform(get("/curvePoint/list"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    void should_Return_CurvePoint_List_User_Auth() throws Exception {
        when(curveService.findAll()).thenReturn(curvePointList);
        mockMvc.perform(get("/curvePoint/list"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("curvePointList"));
    }

    @Test
    @WithAnonymousUser
    void should_Return_CurvePoint_Form_No_Auth() throws Exception {
        mockMvc.perform(get("/curvePoint/add"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    void should_Return_CurvePoint_Form_User_Auth() throws Exception {
        mockMvc.perform(get("/curvePoint/add"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("curvePoint"));
    }

    @Test
    @WithAnonymousUser
    void addCurvePointNoAuth() throws Exception {
        CurvePoint curvePoint = new CurvePoint(1,14d,10.0d);
        mockMvc.perform(post("/curvePoint/validate").flashAttr("curvePoint", curvePoint))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    void addCurvePointWithErrorInFieldUserAuth() throws Exception {
        CurvePoint curvePoint = new CurvePoint(null,14d,10.0d);
        mockMvc.perform(post("/curvePoint/validate").flashAttr("curvePoint", curvePoint))
                .andExpect(status().isOk())
                .andExpect(model().hasErrors());
    }

    @Test
    @WithMockUser
    void addCurvePointWithNoErrorInField() throws Exception {
        CurvePoint curvePoint = new CurvePoint(1,14d,10.0d);
        mockMvc.perform(post("/curvePoint/validate")
                                .flashAttr("curvePoint",
                                           curvePoint))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @WithAnonymousUser
    void showUpdateFormNoAuth() throws Exception {
        CurvePoint curvePoint = new CurvePoint(1,14d,10.0d);
        when(curveService.findById(anyInt())).thenReturn(curvePoint);
        mockMvc.perform(get("/curvePoint/update/1").flashAttr("curvePoint", curvePoint))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    void showUpdateFormUserAuth() throws Exception {
        CurvePoint curvePoint = new CurvePoint(1,14d,10.0d);
        when(curveService.findById(anyInt())).thenReturn(curvePoint);
        mockMvc.perform(get("/curvePoint/update/1").flashAttr("curvePoint", curvePoint))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("curvePoint"));
    }

    @Test
    @WithAnonymousUser
    void saveUpdateNoAuth() throws Exception {
        CurvePoint curvePoint = new CurvePoint(1,14d,10.0d);
        when(curveService.findById(anyInt())).thenReturn(curvePoint);
        mockMvc.perform(post("/curvePoint/update/1").flashAttr("curvePoint", curvePoint))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    void saveUpdateWithErrorInFieldsUserAuth() throws Exception {
        CurvePoint curvePoint = new CurvePoint(1,-1.00,10.0d);
        when(curveService.findById(anyInt())).thenReturn(curvePoint);
        mockMvc.perform(post("/curvePoint/update/1").flashAttr("curvePoint", curvePoint))
                .andExpect(status().isOk())
                .andExpect(model().hasErrors());
    }

    @Test
    @WithMockUser
    void saveUpdateWithNoErrorInFieldsUserAuth() throws Exception {
        CurvePoint curvePoint = new CurvePoint(1,14.00,10.00);
        when(curveService.findById(anyInt())).thenReturn(curvePoint);
        mockMvc.perform(post("/curvePoint/update/1").flashAttr("curvePoint", curvePoint))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @WithAnonymousUser
    void deleteNoAuth() throws Exception {
        mockMvc.perform(get("/curvePoint/delete/1"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    void deleteUserAuth() throws Exception {
        mockMvc.perform(get("/curvePoint/delete/1"))
                .andExpect(status().is3xxRedirection());
    }

}
