package com.nnk.springboot.controllers;

import com.nnk.springboot.domain.BidList;
import com.nnk.springboot.domain.User;
import com.nnk.springboot.exceptions.UserAlreadyExistingException;
import com.nnk.springboot.services.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import java.util.ArrayList;
import java.util.List;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UserController.class)
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    private static List<User> userList;

    @BeforeEach
    void setUpPerTest() {
        userList = new ArrayList<>();
        userList.add(new User("testusername", "test test", "USER"));
        userList.add(new User("testusername", "test test", "USER"));
    }

    @Test
    @WithAnonymousUser
    void should_Return_Unauthorized_No_Auth() throws Exception {
        mockMvc.perform(get("/user/list"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void should_Return_User_List_User_Auth() throws Exception {
        when(userService.findAll()).thenReturn(userList);
        mockMvc.perform(get("/user/list"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("userList"));
    }

    @Test
    @WithAnonymousUser
    void should_Return_User_Form_No_Auth() throws Exception {
        mockMvc.perform(get("/user/add"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void should_Return_User_Form_User_Auth() throws Exception {
        mockMvc.perform(get("/user/add"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("user"));
    }

    @Test
    @WithAnonymousUser
    void addBidListNoAuth() throws Exception {
        BidList bidList = new BidList(null, "test", 123.23);
        mockMvc.perform(post("/bidList/validate").flashAttr("bidList", bidList))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void addUserWithErrorInFieldUserAuth() throws Exception {
        User user = new User("", "test test", "USER");
        mockMvc.perform(post("/user/validate").flashAttr("user", user))
                .andExpect(status().isOk())
                .andExpect(model().hasErrors());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void addUserWithNoErrorInField() throws Exception {
        User user = new User("testusername", "test test", "USER");
        user.setPassword("Test1234$");
        mockMvc.perform(post("/user/validate")
                                .flashAttr("user",
                                           user))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void addUserAlreadyExisting() throws Exception {
        User user = new User("testusername", "test test", "USER");
        user.setPassword("Test1234$");
        doThrow(UserAlreadyExistingException.class).when(userService)
                .save(user);
        mockMvc.perform(post("/user/validate")
                                .flashAttr("user",
                                           user))
                .andExpect(status().isOk());
    }

    @Test
    @WithAnonymousUser
    void showUpdateFormNoAuth() throws Exception {
        User user = new User("testusername", "test test", "USER");
        when(userService.findById(anyInt())).thenReturn(user);
        mockMvc.perform(get("/user/update/1").flashAttr("user", user))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void showUpdateFormUserAuth() throws Exception {
        User user = new User("testusername", "test test", "USER");
        when(userService.findById(anyInt())).thenReturn(user);
        mockMvc.perform(get("/user/update/1").flashAttr("user", user))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("user"));
    }

    @Test
    @WithAnonymousUser
    void saveUpdateNoAuth() throws Exception {
        User user = new User("testusername", "test test", "USER");
        when(userService.findById(anyInt())).thenReturn(user);
        mockMvc.perform(post("/user/update/1").flashAttr("user", user))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void saveUpdateWithErrorInFieldsUserAuth() throws Exception {
        User user = new User("", "test test", "USER");
        when(userService.findById(anyInt())).thenReturn(user);
        mockMvc.perform(post("/user/update/1").flashAttr("user", user))
                .andExpect(status().isOk())
                .andExpect(model().hasErrors());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void updateUserAlreadyExisting() throws Exception {
        User user = new User("testusername", "test test", "USER");
        user.setPassword("Test1234$");
        doThrow(UserAlreadyExistingException.class).when(userService)
                .update(user);
        mockMvc.perform(post("/user/update/1")
                                .flashAttr("user",
                                           user))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void saveUpdateWithNoErrorInFieldsUserAuth() throws Exception {
        User user = new User("testusername", "test test", "USER");
        user.setPassword("Test1234$");
        when(userService.findById(anyInt())).thenReturn(user);
        mockMvc.perform(post("/user/update/1").flashAttr("user", user))
                .andExpect(status().is3xxRedirection());
    }

    @Test
    @WithAnonymousUser
    void deleteNoAuth() throws Exception {
        mockMvc.perform(get("/user/delete/1"))
                .andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void deleteUserAuth() throws Exception {
        mockMvc.perform(get("/user/delete/1"))
                .andExpect(status().is3xxRedirection());
    }


}
