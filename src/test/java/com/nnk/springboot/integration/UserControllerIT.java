package com.nnk.springboot.integration;

import com.nnk.springboot.domain.User;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import javax.transaction.Transactional;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.anonymous;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
public class UserControllerIT extends BaseIT {

    @DisplayName("Get user list")
    @Nested
    class GetUserList {

        @Test
        void getUserListNoAuth() throws Exception {
            mockMvc.perform(get("/user/list").with(anonymous()))
                    .andExpect(status().isUnauthorized());
        }

        @Test
        void getUserListUserAuth() throws Exception {
            mockMvc.perform(get("/user/list").with(httpBasic("user", "user")))
                    .andExpect(status().isForbidden());
        }

        @Test
        void getUserListAdminAuth() throws Exception {
            mockMvc.perform(get("/user/list").with(httpBasic("admin", "admin")))
                    .andExpect(status().isOk());
        }

    }

    @DisplayName("Add user form")
    @Nested
    class AddUserForm {

        @Test
        void addUserNoAuth() throws Exception {
            mockMvc.perform(get("/user/add").with(anonymous()))
                    .andExpect(status().isUnauthorized());
        }

        @Test
        void addUserUserAuth() throws Exception {
            mockMvc.perform(get("/user/add").with(httpBasic("user", "user")))
                    .andExpect(status().isForbidden());
        }

        @Test
        void addUserAdminAuth() throws Exception {
            mockMvc.perform(get("/user/add").with(httpBasic("admin", "admin")))
                    .andExpect(status().isOk());
        }
    }

    @DisplayName("Add user")
    @Nested
    @Transactional
    class AddUser {

        @Test
        void addUserNoAuth() throws Exception {
            mockMvc.perform(post("/user/validate").with(anonymous()))
                    .andExpect(status().isUnauthorized());
        }

        @Test
        void addUserWithNoErrorInFieldUserAuth() throws Exception {
            mockMvc.perform(post("/user/validate").with(httpBasic("user", "user")))
                    .andExpect(status().isForbidden());
        }

        @Rollback
        @Test
        void addUserWithNoErrorInFieldAdminAuth() throws Exception {
            User user = new User("Test1234", "Test Test", "USER");
            user.setPassword("Test1234!");
            mockMvc.perform(post("/user/validate").with(httpBasic("admin", "admin"))
                                    .flashAttr("user", user))
                    .andExpect(status().is3xxRedirection());
        }

        @Test
        void addUserAlreadyExistingAdminAuth() throws Exception {
            User user = new User("user", "Test Test", "USER");
            user.setPassword("Test1234!");
            mockMvc.perform(post("/user/validate").with(httpBasic("admin", "admin"))
                                    .flashAttr("user", user))
                    .andExpect(status().isOk())
                    .andExpect(model().attributeExists("userError"));
        }

        @Test
        void addUserWithErrorInFieldAdminAuth() throws Exception {
            User user = new User("user", "Test Test", "USER");
            user.setPassword("test");
            mockMvc.perform(post("/user/validate").with(httpBasic("admin", "admin"))
                                    .flashAttr("user", user))
                    .andExpect(status().isOk())
                    .andExpect(model().hasErrors());
        }
    }

    @DisplayName("Update user")
    @Nested
    class UpdateBidListForm {

        @Test
        void updateUserNoAuth() throws Exception {
            mockMvc.perform(get("/user/update/1").with(anonymous()))
                    .andExpect(status().isUnauthorized());
        }

        @Test
        void updateUserUserAuth() throws Exception {
            mockMvc.perform(get("/user/update/1").with(httpBasic("user", "user")))
                    .andExpect(status().isForbidden());
        }

        @Test
        void updateUserAdminAuth() throws Exception {
            mockMvc.perform(get("/user/update/1").with(httpBasic("admin", "admin")))
                    .andExpect(status().isOk());
        }
    }

    @DisplayName("Update user")
    @Nested
    @Transactional
    class UpdateBidList {

        @Test
        void updateUserNoAuth() throws Exception {
            mockMvc.perform(post("/user/update/1").with(anonymous()))
                    .andExpect(status().isUnauthorized());
        }

        @Test
        void updateUserUserAuth() throws Exception {
            mockMvc.perform(post("/user/update/1").with(httpBasic("user", "user")))
                    .andExpect(status().isForbidden());
        }

        @Rollback
        @Test
        void updateUserAdminAuth() throws Exception {
            User user = new User("Test1234", "Test Test", "USER");
            user.setPassword("Test1234!");
            mockMvc.perform(post("/user/update/1").with(httpBasic("admin", "admin"))
                                    .flashAttr("user", user))
                    .andExpect(status().is3xxRedirection());
        }

        @Test
        void updateUserWithErrorInFieldUserAuth() throws Exception {
            User user = new User("Test1234", "Test Test", "USER");
            user.setPassword("test");
            mockMvc.perform(post("/user/update/1").with(httpBasic("admin", "admin"))
                                    .flashAttr("user", user))
                    .andExpect(status().isOk())
                    .andExpect(model().hasErrors());
        }

        @Test
        void updateUserAlreadyExistingAdminAuth() throws Exception {
            User user = new User("user", "Test Test", "USER");
            user.setPassword("Test1234!");
            mockMvc.perform(post("/user/update/1").with(httpBasic("admin", "admin"))
                                    .flashAttr("user", user))
                    .andExpect(status().isOk())
                    .andExpect(model().attributeExists("userError"));
        }
    }

    @DisplayName("Delete user")
    @Nested
    @Transactional
    class DeleteBidList {

        @Test
        void deleteUserNoAuth() throws Exception {
            mockMvc.perform(get("/user/delete/1").with(anonymous()))
                    .andExpect(status().isUnauthorized());
        }

        @Test
        void deleteUserUserAuth() throws Exception {
            mockMvc.perform(get("/user/delete/1").with(httpBasic("user", "user")))
                    .andExpect(status().isForbidden());
        }

        @Rollback
        @Test
        void deleteUserAdminAuth() throws Exception {
            mockMvc.perform(get("/user/delete/1").with(httpBasic("admin", "admin")))
                    .andExpect(status().is3xxRedirection());
        }
    }

}
