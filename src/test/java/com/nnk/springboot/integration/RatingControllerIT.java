package com.nnk.springboot.integration;

import com.nnk.springboot.domain.Rating;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import javax.transaction.Transactional;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.anonymous;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.oauth2Login;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
public class RatingControllerIT extends BaseIT {

    @DisplayName("Get rating list")
    @Nested
    class GetRatingList {

        @Test
        void getRatingListNoAuth() throws Exception {
            mockMvc.perform(get("/rating/list").with(anonymous()))
                    .andExpect(status().isUnauthorized());
        }

        @Test
        void getRatingListUserAuth() throws Exception {
            mockMvc.perform(get("/rating/list").with(httpBasic("user", "user")))
                    .andExpect(status().isOk());
        }

        @Test
        void getRatingListAdminAuth() throws Exception {
            mockMvc.perform(get("/rating/list").with(httpBasic("admin", "admin")))
                    .andExpect(status().isOk());
        }

        @Test
        void getRatingListOAuth2Auth() throws Exception {
            mockMvc.perform(get("/rating/list").with(oauth2Login()))
                    .andExpect(status().isOk());
        }
    }

    @DisplayName("Add rating form")
    @Nested
    class AddRatingForm {

        @Test
        void addRatingNoAuth() throws Exception {
            mockMvc.perform(get("/rating/add").with(anonymous()))
                    .andExpect(status().isUnauthorized());
        }

        @Test
        void addRatingUserAuth() throws Exception {
            mockMvc.perform(get("/rating/add").with(httpBasic("user", "user")))
                    .andExpect(status().isOk());
        }

        @Test
        void addRatingAdminAuth() throws Exception {
            mockMvc.perform(get("/rating/add").with(httpBasic("admin", "admin")))
                    .andExpect(status().isOk());
        }
    }

    @DisplayName("Add rating")
    @Nested
    @Transactional
    class AddRating {

        @Test
        void addRatingNoAuth() throws Exception {
            mockMvc.perform(post("/rating/validate").with(anonymous()))
                    .andExpect(status().isUnauthorized());
        }

        @Rollback
        @Test
        void addRatingWithNoErrorInFieldUserAuth() throws Exception {
            Rating rating = new Rating("Test", "Test", "Test", 10);
            mockMvc.perform(post("/rating/validate").with(httpBasic("user", "user"))
                                    .flashAttr("rating", rating))
                    .andExpect(status().is3xxRedirection());
        }

        @Rollback
        @Test
        void addRatingWithNoErrorInFieldAdminAuth() throws Exception {
            Rating rating = new Rating("Test", "Test", "Test", 10);
            mockMvc.perform(post("/rating/validate").with(httpBasic("admin", "admin"))
                                    .flashAttr("rating", rating))
                    .andExpect(status().is3xxRedirection());
        }

        @Test
        void addRatingWithErrorInFieldUserAuth() throws Exception {
            Rating rating = new Rating("", "Test", "Test", 10);
            mockMvc.perform(post("/rating/validate").with(httpBasic("user", "user"))
                                    .flashAttr("rating", rating))
                    .andExpect(status().isOk())
                    .andExpect(model().hasErrors());
        }
    }

    @DisplayName("Update rating form")
    @Nested
    class UpdateRatingForm {

        @Test
        void updateRatingNoAuth() throws Exception {
            mockMvc.perform(get("/rating/update/1").with(anonymous()))
                    .andExpect(status().isUnauthorized());
        }

        @Test
        void updateRatingUserAuth() throws Exception {
            mockMvc.perform(get("/rating/update/1").with(httpBasic("user", "user")))
                    .andExpect(status().isOk());
        }

        @Test
        void updateRatingAdminAuth() throws Exception {
            mockMvc.perform(get("/rating/update/1").with(httpBasic("admin", "admin")))
                    .andExpect(status().isOk());
        }
    }

    @DisplayName("Update rating")
    @Nested
    @Transactional
    class UpdateRating {

        @Test
        void updateRatingNoAuth() throws Exception {
            mockMvc.perform(post("/rating/update/1").with(anonymous()))
                    .andExpect(status().isUnauthorized());
        }

        @Rollback
        @Test
        void updateRatingUserAuth() throws Exception {
            Rating rating = new Rating("Test", "Test", "Test", 10);
            mockMvc.perform(post("/rating/update/1").with(httpBasic("user", "user"))
                                    .flashAttr("rating", rating))
                    .andExpect(status().is3xxRedirection());
        }

        @Rollback
        @Test
        void updateRatingAdminAuth() throws Exception {
            Rating rating = new Rating("Test", "Test", "Test", 10);
            mockMvc.perform(post("/rating/update/1").with(httpBasic("admin", "admin"))
                                    .flashAttr("rating", rating))
                    .andExpect(status().is3xxRedirection());
        }

        @Test
        void updateBidListWithErrorInFieldUserAuth() throws Exception {
            Rating rating = new Rating("", "Test", "Test", 10);
            mockMvc.perform(post("/rating/update/1").with(httpBasic("admin", "admin"))
                                    .flashAttr("rating", rating))
                    .andExpect(status().isOk())
                    .andExpect(model().hasErrors());
        }
    }

    @DisplayName("Delete rating")
    @Nested
    @Transactional
    class DeleteRating {

        @Test
        void deleteRatingNoAuth() throws Exception {
            mockMvc.perform(get("/rating/delete/1").with(anonymous()))
                    .andExpect(status().isUnauthorized());
        }

        @Rollback
        @Test
        void deleteRatingUserAuth() throws Exception {
            mockMvc.perform(get("/rating/delete/1").with(httpBasic("user", "user")))
                    .andExpect(status().is3xxRedirection());
        }

        @Rollback
        @Test
        void deleteRatingAdminAuth() throws Exception {
            mockMvc.perform(get("/rating/delete/1").with(httpBasic("admin", "admin")))
                    .andExpect(status().is3xxRedirection());
        }
    }

}
