package com.nnk.springboot.integration;

import com.nnk.springboot.domain.Trade;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import javax.transaction.Transactional;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.anonymous;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.oauth2Login;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
public class TradeControllerIT extends BaseIT {

    @DisplayName("Get trade list")
    @Nested
    class GetTradeList {

        @Test
        void getTradeListNoAuth() throws Exception {
            mockMvc.perform(get("/trade/list").with(anonymous()))
                    .andExpect(status().isUnauthorized());
        }

        @Test
        void getTradeListUserAuth() throws Exception {
            mockMvc.perform(get("/trade/list").with(httpBasic("user", "user")))
                    .andExpect(status().isOk());
        }

        @Test
        void getTradeListAdminAuth() throws Exception {
            mockMvc.perform(get("/trade/list").with(httpBasic("admin", "admin")))
                    .andExpect(status().isOk());
        }

        @Test
        void getTradeListOAuth2Auth() throws Exception {
            mockMvc.perform(get("/trade/list").with(oauth2Login()))
                    .andExpect(status().isOk());
        }
    }

    @DisplayName("Add trade form")
    @Nested
    class AddTradeForm {

        @Test
        void addTradeNoAuth() throws Exception {
            mockMvc.perform(get("/trade/add").with(anonymous()))
                    .andExpect(status().isUnauthorized());
        }

        @Test
        void addTradeUserAuth() throws Exception {
            mockMvc.perform(get("/trade/add").with(httpBasic("user", "user")))
                    .andExpect(status().isOk());
        }

        @Test
        void addTradeAdminAuth() throws Exception {
            mockMvc.perform(get("/trade/add").with(httpBasic("admin", "admin")))
                    .andExpect(status().isOk());
        }
    }

    @DisplayName("Add trade")
    @Nested
    @Transactional
    class AddTrade {

        @Test
        void addTradeNoAuth() throws Exception {
            mockMvc.perform(post("/trade/validate").with(anonymous()))
                    .andExpect(status().isUnauthorized());
        }

        @Rollback
        @Test
        void addTradeWithNoErrorInFieldUserAuth() throws Exception {
            Trade trade = new Trade("Test", "Test", 10.00);
            mockMvc.perform(post("/trade/validate").with(httpBasic("user", "user"))
                                    .flashAttr("trade", trade))
                    .andExpect(status().is3xxRedirection());
        }

        @Rollback
        @Test
        void addTradeWithNoErrorInFieldAdminAuth() throws Exception {
            Trade trade = new Trade("Test", "Test", 10.00);
            mockMvc.perform(post("/trade/validate").with(httpBasic("admin", "admin"))
                                    .flashAttr("trade", trade))
                    .andExpect(status().is3xxRedirection());
        }

        @Test
        void addTradeWithErrorInFieldUserAuth() throws Exception {
            Trade trade = new Trade("Test", "Test", -10.00);
            mockMvc.perform(post("/trade/validate").with(httpBasic("user", "user"))
                                    .flashAttr("trade", trade))
                    .andExpect(status().isOk())
                    .andExpect(model().hasErrors());
        }
    }

    @DisplayName("Update trade form")
    @Nested
    class UpdateTradeForm {

        @Test
        void updateTradeNoAuth() throws Exception {
            mockMvc.perform(get("/trade/update/1").with(anonymous()))
                    .andExpect(status().isUnauthorized());
        }

        @Test
        void updateTradeUserAuth() throws Exception {
            mockMvc.perform(get("/trade/update/1").with(httpBasic("user", "user")))
                    .andExpect(status().isOk());
        }

        @Test
        void updateTradeAdminAuth() throws Exception {
            mockMvc.perform(get("/trade/update/1").with(httpBasic("admin", "admin")))
                    .andExpect(status().isOk());
        }
    }

    @DisplayName("Update trade")
    @Nested
    @Transactional
    class UpdateTrade {

        @Test
        void updateTradeNoAuth() throws Exception {
            mockMvc.perform(post("/trade/update/1").with(anonymous()))
                    .andExpect(status().isUnauthorized());
        }

        @Rollback
        @Test
        void updateTradeUserAuth() throws Exception {
            Trade trade = new Trade("Test", "Test", 10.00);
            mockMvc.perform(post("/trade/update/1").with(httpBasic("user", "user"))
                                    .flashAttr("trade", trade))
                    .andExpect(status().is3xxRedirection());
        }

        @Rollback
        @Test
        void updateTradeAdminAuth() throws Exception {
            Trade trade = new Trade("Test", "Test", 10.00);
            mockMvc.perform(post("/trade/update/1").with(httpBasic("admin", "admin"))
                                    .flashAttr("trade", trade))
                    .andExpect(status().is3xxRedirection());
        }

        @Test
        void updateTradeWithErrorInFieldUserAuth() throws Exception {
            Trade trade = new Trade("Test", "Test", -10.00);
            mockMvc.perform(post("/trade/update/1").with(httpBasic("admin", "admin"))
                                    .flashAttr("trade", trade))
                    .andExpect(status().isOk())
                    .andExpect(model().hasErrors());
        }
    }

    @DisplayName("Delete trade")
    @Nested
    @Transactional
    class DeleteBidList {

        @Test
        void deleteTradeNoAuth() throws Exception {
            mockMvc.perform(get("/trade/delete/1").with(anonymous()))
                    .andExpect(status().isUnauthorized());
        }

        @Rollback
        @Test
        void deleteTradeUserAuth() throws Exception {
            mockMvc.perform(get("/trade/delete/1").with(httpBasic("user", "user")))
                    .andExpect(status().is3xxRedirection());
        }

        @Rollback
        @Test
        void deleteTradeAdminAuth() throws Exception {
            mockMvc.perform(get("/trade/delete/1").with(httpBasic("admin", "admin")))
                    .andExpect(status().is3xxRedirection());
        }
    }
}
