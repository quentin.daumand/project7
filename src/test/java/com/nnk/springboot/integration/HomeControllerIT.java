package com.nnk.springboot.integration;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.anonymous;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
public class HomeControllerIT extends BaseIT {

    @DisplayName("Get home page")
    @Nested
    class GetHomePage {

        @Test
        void getHomePageNoAuth() throws Exception {
            mockMvc.perform(get("/").with(anonymous()))
                    .andExpect(status().isOk());
        }

        @Test
        void getHomePageUserAuth() throws Exception {
            mockMvc.perform(get("/").with(httpBasic("user", "user")))
                    .andExpect(status().isForbidden());
        }

        @Test
        void getHomePageAdminAuth() throws Exception {
            mockMvc.perform(get("/").with(httpBasic("admin", "admin")))
                    .andExpect(status().isForbidden());
        }
    }

    @DisplayName("Get admin page")
    @Nested
    class GetAdminPage {

        @Test
        void getAdminPageNoAuth() throws Exception {
            mockMvc.perform(get("/admin/home").with(anonymous()))
                    .andExpect(status().isUnauthorized());
        }

        @Test
        void getAdminPageUserAuth() throws Exception {
            mockMvc.perform(get("/admin/home").with(httpBasic("user", "user")))
                    .andExpect(status().isForbidden());
        }

        @Test
        void getAdminPageAdminAuth() throws Exception {
            mockMvc.perform(get("/admin/home").with(httpBasic("admin", "admin")))
                    .andExpect(status().is3xxRedirection());
        }
    }

}
