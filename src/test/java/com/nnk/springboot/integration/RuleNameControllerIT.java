package com.nnk.springboot.integration;

import com.nnk.springboot.domain.RuleName;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import javax.transaction.Transactional;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.anonymous;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.oauth2Login;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
public class RuleNameControllerIT extends BaseIT {

    @DisplayName("Get rule name list")
    @Nested
    class GetRuleNameList {

        @Test
        void getRuleNameListNoAuth() throws Exception {
            mockMvc.perform(get("/ruleName/list").with(anonymous()))
                    .andExpect(status().isUnauthorized());
        }

        @Test
        void getRuleNameUserAuth() throws Exception {
            mockMvc.perform(get("/ruleName/list").with(httpBasic("user", "user")))
                    .andExpect(status().isOk());
        }

        @Test
        void getRuleNameAdminAuth() throws Exception {
            mockMvc.perform(get("/ruleName/list").with(httpBasic("admin", "admin")))
                    .andExpect(status().isOk());
        }

        @Test
        void getRuleNameOAuth2Auth() throws Exception {
            mockMvc.perform(get("/ruleName/list").with(oauth2Login()))
                    .andExpect(status().isOk());
        }
    }

    @DisplayName("Add rule name form")
    @Nested
    class AddRuleNameForm {

        @Test
        void addRuleNameNoAuth() throws Exception {
            mockMvc.perform(get("/ruleName/add").with(anonymous()))
                    .andExpect(status().isUnauthorized());
        }

        @Test
        void addRuleNameUserAuth() throws Exception {
            mockMvc.perform(get("/ruleName/add").with(httpBasic("user", "user")))
                    .andExpect(status().isOk());
        }

        @Test
        void addRuleNameAdminAuth() throws Exception {
            mockMvc.perform(get("/ruleName/add").with(httpBasic("admin", "admin")))
                    .andExpect(status().isOk());
        }
    }

    @DisplayName("Add rule name")
    @Nested
    @Transactional
    class AddRuleName {

        @Test
        void addRuleNameNoAuth() throws Exception {
            mockMvc.perform(post("/ruleName/validate").with(anonymous()))
                    .andExpect(status().isUnauthorized());
        }

        @Rollback
        @Test
        void addRuleNameWithNoErrorInFieldUserAuth() throws Exception {
            RuleName ruleName = new RuleName("Test", "Test", "Test", "Test", "Test", "Test");
            mockMvc.perform(post("/ruleName/validate").with(httpBasic("user", "user"))
                                    .flashAttr("ruleName", ruleName))
                    .andExpect(status().is3xxRedirection());
        }

        @Rollback
        @Test
        void addRuleNameWithNoErrorInFieldAdminAuth() throws Exception {
            RuleName ruleName = new RuleName("Test", "Test", "Test", "Test", "Test", "Test");
            mockMvc.perform(post("/ruleName/validate").with(httpBasic("admin", "admin"))
                                    .flashAttr("ruleName", ruleName))
                    .andExpect(status().is3xxRedirection());
        }

        @Test
        void addRuleNameWithErrorInFieldUserAuth() throws Exception {
            RuleName ruleName = new RuleName("", "Test", "Test", "Test", "Test", "Test");
            mockMvc.perform(post("/ruleName/validate").with(httpBasic("user", "user"))
                                    .flashAttr("ruleName", ruleName))
                    .andExpect(status().isOk())
                    .andExpect(model().hasErrors());
        }
    }

    @DisplayName("Update rule name form")
    @Nested
    class UpdateRuleNameForm {

        @Test
        void updateRuleNameNoAuth() throws Exception {
            mockMvc.perform(get("/ruleName/update/1").with(anonymous()))
                    .andExpect(status().isUnauthorized());
        }

        @Test
        void updateRuleNameUserAuth() throws Exception {
            mockMvc.perform(get("/ruleName/update/1").with(httpBasic("user", "user")))
                    .andExpect(status().isOk());
        }

        @Test
        void updateRuleNameAdminAuth() throws Exception {
            mockMvc.perform(get("/ruleName/update/1").with(httpBasic("admin", "admin")))
                    .andExpect(status().isOk());
        }
    }

    @DisplayName("Update rule name")
    @Nested
    @Transactional
    class UpdateRuleName {

        @Test
        void updateRuleNameNoAuth() throws Exception {
            mockMvc.perform(post("/ruleName/update/1").with(anonymous()))
                    .andExpect(status().isUnauthorized());
        }

        @Rollback
        @Test
        void updateRuleNameUserAuth() throws Exception {
            RuleName ruleName = new RuleName("Test","Test","Test","Test","Test","Test");
            mockMvc.perform(post("/ruleName/update/1").with(httpBasic("user", "user"))
                                    .flashAttr("ruleName", ruleName))
                    .andExpect(status().is3xxRedirection());
        }

        @Rollback
        @Test
        void updateRuleNameAdminAuth() throws Exception {
            RuleName ruleName = new RuleName("Test","Test","Test","Test","Test","Test");
            mockMvc.perform(post("/ruleName/update/1").with(httpBasic("admin", "admin"))
                                    .flashAttr("ruleName", ruleName))
                    .andExpect(status().is3xxRedirection());
        }

        @Test
        void updateRuleNameWithErrorInFieldUserAuth() throws Exception {
            RuleName ruleName = new RuleName("","Test","Test","Test","Test","Test");
            mockMvc.perform(post("/ruleName/update/1").with(httpBasic("admin", "admin"))
                                    .flashAttr("ruleName", ruleName))
                    .andExpect(status().isOk())
                    .andExpect(model().hasErrors());
        }
    }

    @DisplayName("Delete rule name")
    @Nested
    @Transactional
    class DeleteRuleName {

        @Test
        void deleteRuleNameNoAuth() throws Exception {
            mockMvc.perform(get("/ruleName/delete/1").with(anonymous()))
                    .andExpect(status().isUnauthorized());
        }

        @Rollback
        @Test
        void deleteRuleNameUserAuth() throws Exception {
            mockMvc.perform(get("/ruleName/delete/1").with(httpBasic("user", "user")))
                    .andExpect(status().is3xxRedirection());
        }

        @Rollback
        @Test
        void deleteRuleNameAdminAuth() throws Exception {
            mockMvc.perform(get("/ruleName/delete/1").with(httpBasic("admin", "admin")))
                    .andExpect(status().is3xxRedirection());
        }
    }
}
