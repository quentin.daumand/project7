package com.nnk.springboot.integration;

import com.nnk.springboot.domain.BidList;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import javax.transaction.Transactional;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.anonymous;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.oauth2Login;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
public class BidListControllerIT extends BaseIT {

    @DisplayName("Get bid lists")
    @Nested
    class GetBidLists {

        @Test
        void getBidListNoAuth() throws Exception {
            mockMvc.perform(get("/bidList/list").with(anonymous()))
                    .andExpect(status().isUnauthorized());
        }

        @Test
        void getBidListUserAuth() throws Exception {
            mockMvc.perform(get("/bidList/list").with(httpBasic("user", "user")))
                    .andExpect(status().isOk());
        }

        @Test
        void getBidListAdminAuth() throws Exception {
            mockMvc.perform(get("/bidList/list").with(httpBasic("admin", "admin")))
                    .andExpect(status().isOk());
        }

        @Test
        void getBidListOAuth2Auth() throws Exception {
            mockMvc.perform(get("/bidList/list").with(oauth2Login()))
                    .andExpect(status().isOk());
        }
    }

    @DisplayName("Add bid list form")
    @Nested
    class AddBidListForm {

        @Test
        void addBidListNoAuth() throws Exception {
            mockMvc.perform(get("/bidList/add").with(anonymous()))
                    .andExpect(status().isUnauthorized());
        }

        @Test
        void addBidListUserAuth() throws Exception {
            mockMvc.perform(get("/bidList/add").with(httpBasic("user", "user")))
                    .andExpect(status().isOk());
        }

        @Test
        void addBidListAdminAuth() throws Exception {
            mockMvc.perform(get("/bidList/add").with(httpBasic("admin", "admin")))
                    .andExpect(status().isOk());
        }
    }

    @DisplayName("Add bid list")
    @Nested
    @Transactional
    class AddBidList {

        @Test
        void addBidListNoAuth() throws Exception {
            mockMvc.perform(post("/bidList/validate").with(anonymous()))
                    .andExpect(status().isUnauthorized());
        }

        @Rollback
        @Test
        void addBidListWithNoErrorInFieldUserAuth() throws Exception {
            BidList bidList = new BidList("account", "type", 10.00);
            mockMvc.perform(post("/bidList/validate").with(httpBasic("user", "user"))
                                    .flashAttr("bidList", bidList))
                    .andExpect(status().is3xxRedirection());
        }

        @Rollback
        @Test
        void addBidListWithNoErrorInFieldAdminAuth() throws Exception {
            BidList bidList = new BidList("account", "type", 10.00);
            mockMvc.perform(post("/bidList/validate").with(httpBasic("admin", "admin"))
                                    .flashAttr("bidList", bidList))
                    .andExpect(status().is3xxRedirection());
        }

        @Test
        void addBidListWithErrorInFieldUserAuth() throws Exception {
            BidList bidList = new BidList("", "type", 10.00);
            mockMvc.perform(post("/bidList/validate").with(httpBasic("user", "user"))
                                    .flashAttr("bidList", bidList))
                    .andExpect(status().isOk())
                    .andExpect(model().hasErrors());
        }
    }

    @DisplayName("Update bid list form")
    @Nested
    class UpdateBidListForm {

        @Test
        void updateBidListNoAuth() throws Exception {
            mockMvc.perform(get("/bidList/update/1").with(anonymous()))
                    .andExpect(status().isUnauthorized());
        }

        @Test
        void updateBidListUserAuth() throws Exception {
            mockMvc.perform(get("/bidList/update/1").with(httpBasic("user", "user")))
                    .andExpect(status().isOk());
        }

        @Test
        void updateBidListAdminAuth() throws Exception {
            mockMvc.perform(get("/bidList/update/1").with(httpBasic("admin", "admin")))
                    .andExpect(status().isOk());
        }
    }

    @DisplayName("Update bid list")
    @Nested
    @Transactional
    class UpdateBidList {

        @Test
        void updateBidListNoAuth() throws Exception {
            mockMvc.perform(post("/bidList/update/1").with(anonymous()))
                    .andExpect(status().isUnauthorized());
        }

        @Rollback
        @Test
        void updateBidListUserAuth() throws Exception {
            BidList bidList = new BidList("account", "type", 10.00);
            mockMvc.perform(post("/bidList/update/1").with(httpBasic("user", "user"))
                                    .flashAttr("bidList", bidList))
                    .andExpect(status().is3xxRedirection());
        }

        @Rollback
        @Test
        void updateBidListAdminAuth() throws Exception {
            BidList bidList = new BidList("account", "type", 10.00);
            mockMvc.perform(post("/bidList/update/1").with(httpBasic("admin", "admin"))
                                    .flashAttr("bidList", bidList))
                    .andExpect(status().is3xxRedirection());
        }

        @Test
        void updateBidListWithErrorInFieldUserAuth() throws Exception {
            BidList bidList = new BidList("", "type", 10.00);
            mockMvc.perform(post("/bidList/update/1").with(httpBasic("admin", "admin"))
                                    .flashAttr("bidList", bidList))
                    .andExpect(status().isOk())
                    .andExpect(model().hasErrors());
        }
    }

    @DisplayName("Delete bid list")
    @Nested
    @Transactional
    class DeleteBidList {

        @Test
        void deleteBidListNoAuth() throws Exception {
            mockMvc.perform(get("/bidList/delete/1").with(anonymous()))
                    .andExpect(status().isUnauthorized());
        }

        @Rollback
        @Test
        void deleteBidListUserAuth() throws Exception {
            mockMvc.perform(get("/bidList/delete/1").with(httpBasic("user", "user")))
                    .andExpect(status().is3xxRedirection());
        }

        @Rollback
        @Test
        void deleteBidListAdminAuth() throws Exception {
            mockMvc.perform(get("/bidList/delete/1").with(httpBasic("admin", "admin")))
                    .andExpect(status().is3xxRedirection());
        }
    }


}
