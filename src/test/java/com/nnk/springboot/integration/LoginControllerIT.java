package com.nnk.springboot.integration;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.anonymous;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.oauth2Login;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
public class LoginControllerIT extends BaseIT {

    @DisplayName("Get error page")
    @Nested
    class GetErrorPage {

        @Test
        void getErrorPageNoAuth() throws Exception {
            mockMvc.perform(get("/app/error").with(anonymous()))
                    .andExpect(status().isUnauthorized());
        }

        @Test
        void getErrorPageUserAuth() throws Exception {
            mockMvc.perform(get("/app/error").with(httpBasic("user", "user")))
                    .andExpect(status().isOk());
        }

        @Test
        void getErrorPageAdminAuth() throws Exception {
            mockMvc.perform(get("/app/error").with(httpBasic("admin", "admin")))
                    .andExpect(status().isOk());
        }

        @Test
        void getErrorPageOAuth2Auth() throws Exception {
            mockMvc.perform(get("/app/error").with(oauth2Login()))
                    .andExpect(status().isOk());
        }
    }

}
