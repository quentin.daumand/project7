package com.nnk.springboot.integration;

import com.nnk.springboot.domain.CurvePoint;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import javax.transaction.Transactional;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.anonymous;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.oauth2Login;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
public class CurveControllerIT extends BaseIT {


    @DisplayName("Get curve point list")
    @Nested
    class GetCurvePointList {

        @Test
        void getCurvePointListNoAuth() throws Exception {
            mockMvc.perform(get("/curvePoint/list").with(anonymous()))
                    .andExpect(status().isUnauthorized());
        }

        @Test
        void getCurvePointListUserAuth() throws Exception {
            mockMvc.perform(get("/curvePoint/list").with(httpBasic("user", "user")))
                    .andExpect(status().isOk());
        }

        @Test
        void getCurvePointListAdminAuth() throws Exception {
            mockMvc.perform(get("/curvePoint/list").with(httpBasic("admin", "admin")))
                    .andExpect(status().isOk());
        }

        @Test
        void getCurvePointListOAuth2Auth() throws Exception {
            mockMvc.perform(get("/curvePoint/list").with(oauth2Login()))
                    .andExpect(status().isOk());
        }
    }

    @DisplayName("Add curve point form")
    @Nested
    class AddCurvePointForm {

        @Test
        void addCurvePointNoAuth() throws Exception {
            mockMvc.perform(get("/curvePoint/add").with(anonymous()))
                    .andExpect(status().isUnauthorized());
        }

        @Test
        void addCurvePointUserAuth() throws Exception {
            mockMvc.perform(get("/curvePoint/add").with(httpBasic("user", "user")))
                    .andExpect(status().isOk());
        }

        @Test
        void addCurvePointAdminAuth() throws Exception {
            mockMvc.perform(get("/curvePoint/add").with(httpBasic("admin", "admin")))
                    .andExpect(status().isOk());
        }
    }

    @DisplayName("Add curve point")
    @Nested
    @Transactional
    class AddCurvePoint {

        @Test
        void addCurvePointNoAuth() throws Exception {
            mockMvc.perform(post("/curvePoint/validate").with(anonymous()))
                    .andExpect(status().isUnauthorized());
        }

        @Rollback
        @Test
        void addCurvePointWithNoErrorInFieldUserAuth() throws Exception {
            CurvePoint curvePoint = new CurvePoint(10, 10.00, 10.00);
            mockMvc.perform(post("/curvePoint/validate").with(httpBasic("user", "user"))
                                    .flashAttr("curvePoint", curvePoint))
                    .andExpect(status().is3xxRedirection());
        }

        @Rollback
        @Test
        void addCurvePointWithNoErrorInFieldAdminAuth() throws Exception {
            CurvePoint curvePoint = new CurvePoint(10, 10.00, 10.00);
            mockMvc.perform(post("/curvePoint/validate").with(httpBasic("admin", "admin"))
                                    .flashAttr("curvePoint", curvePoint))
                    .andExpect(status().is3xxRedirection());
        }

        @Test
        void addCurvePointWithErrorInFieldUserAuth() throws Exception {
            CurvePoint curvePoint = new CurvePoint(null, 10.00, 10.00);
            mockMvc.perform(post("/curvePoint/validate").with(httpBasic("user", "user"))
                                    .flashAttr("curvePoint", curvePoint))
                    .andExpect(status().isOk())
                    .andExpect(model().hasErrors());
        }
    }

    @DisplayName("Update curve point form")
    @Nested
    class UpdateCurvePointForm {

        @Test
        void updateCurvePointNoAuth() throws Exception {
            mockMvc.perform(get("/curvePoint/update/1").with(anonymous()))
                    .andExpect(status().isUnauthorized());
        }

        @Test
        void updateCurvePointUserAuth() throws Exception {
            mockMvc.perform(get("/curvePoint/update/1").with(httpBasic("user", "user")))
                    .andExpect(status().isOk());
        }

        @Test
        void updateCurvePointAdminAuth() throws Exception {
            mockMvc.perform(get("/curvePoint/update/1").with(httpBasic("admin", "admin")))
                    .andExpect(status().isOk());
        }
    }

    @DisplayName("Update curve point")
    @Nested
    @Transactional
    class UpdateCurvePoint {

        @Test
        void updateCurvePointNoAuth() throws Exception {
            mockMvc.perform(post("/curvePoint/update/1").with(anonymous()))
                    .andExpect(status().isUnauthorized());
        }

        @Rollback
        @Test
        void updateCurvePointUserAuth() throws Exception {
            CurvePoint curvePoint = new CurvePoint(10, 10.00, 10.00);
            mockMvc.perform(post("/curvePoint/update/1").with(httpBasic("user", "user"))
                                    .flashAttr("curvePoint", curvePoint))
                    .andExpect(status().is3xxRedirection());
        }

        @Rollback
        @Test
        void updateCurvePointAdminAuth() throws Exception {
            CurvePoint curvePoint = new CurvePoint(10, 10.00, 10.00);
            mockMvc.perform(post("/curvePoint/update/1").with(httpBasic("admin", "admin"))
                                    .flashAttr("curvePoint", curvePoint))
                    .andExpect(status().is3xxRedirection());
        }

        @Test
        void updateCurvePointWithErrorInFieldUserAuth() throws Exception {
            CurvePoint curvePoint = new CurvePoint(null, 10.00, 10.00);
            mockMvc.perform(post("/curvePoint/update/1").with(httpBasic("admin", "admin"))
                                    .flashAttr("curvePoint", curvePoint))
                    .andExpect(status().isOk())
                    .andExpect(model().hasErrors());
        }
    }

    @DisplayName("Delete curve point")
    @Nested
    @Transactional
    class DeleteCurvePoint {

        @Test
        void deleteCurvePointNoAuth() throws Exception {
            mockMvc.perform(get("/curvePoint/delete/1").with(anonymous()))
                    .andExpect(status().isUnauthorized());
        }

        @Rollback
        @Test
        void deleteCurvePointUserAuth() throws Exception {
            mockMvc.perform(get("/curvePoint/delete/1").with(httpBasic("user", "user")))
                    .andExpect(status().is3xxRedirection());
        }

        @Rollback
        @Test
        void deleteBidListAdminAuth() throws Exception {
            mockMvc.perform(get("/curvePoint/delete/1").with(httpBasic("admin", "admin")))
                    .andExpect(status().is3xxRedirection());
        }


    }
}
