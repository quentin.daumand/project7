package com.nnk.springboot.services;

import com.nnk.springboot.domain.BidList;
import com.nnk.springboot.domain.Rating;
import com.nnk.springboot.repositories.RatingRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class RatingServiceTest {

    @InjectMocks
    private RatingService ratingService;

    @Mock
    private RatingRepository ratingRepository;

    private static List<Rating> ratingList;

    @BeforeEach
    void setUpPerTest() {
        ratingList = new ArrayList<>();
        ratingList.add(new Rating("test", "test", "test", 1));
        ratingList.add(new Rating("test", "test", "test", 1));
    }

    @Test
    void should_Return_All_Ratings() {
        when(ratingRepository.findAll()).thenReturn(ratingList);
        ratingService.findAll();
        verify(ratingRepository).findAll();
    }

    @Test
    void should_Save_Rating() {
        Rating rating = new Rating("test", "test", "test", 1);
        when(ratingRepository.save(rating)).thenReturn(rating);
        ratingService.save(rating);
        verify(ratingRepository).save(rating);
    }

    @Test
    void should_Return_Rating_By_Id() {
        Rating rating = new Rating("test", "test", "test", 1);
        when(ratingRepository.findById(1)).thenReturn(Optional.of(rating));
        ratingService.findById(1);
        verify(ratingRepository).findById(1);
    }

    @Test
    void should_Delete_Rating_By_Id(){
        doNothing().when(ratingRepository).deleteById(1);
        ratingService.deleteById(1);
        verify(ratingRepository).deleteById(1);
    }

}
