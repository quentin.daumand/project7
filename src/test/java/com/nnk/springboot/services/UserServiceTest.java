package com.nnk.springboot.services;

import com.nnk.springboot.domain.User;
import com.nnk.springboot.exceptions.UserAlreadyExistingException;
import com.nnk.springboot.repositories.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @InjectMocks
    private UserService userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    private static List<User> userList;

    @BeforeEach
    void setUpPerTest() {
        userList = new ArrayList<>();
        userList.add(new User("testusername", "test test", "USER"));
        userList.add(new User("testusername", "test test", "USER"));
    }

    @Test
    void should_Return_All_Users() {
        when(userRepository.findAll()).thenReturn(userList);
        userService.findAll();
        verify(userRepository).findAll();
    }

    @Test
    void should_Save_User() throws UserAlreadyExistingException {
        User user = new User("testusername", "test test", "USER");
        user.setPassword("test");
        when(passwordEncoder.encode(anyString())).thenReturn("test");
        when(userRepository.save(user)).thenReturn(user);
        userService.save(user);
        verify(userRepository).save(user);
    }

    @Test
    void save_Should_Throws_UserAlreadyExistingException() {
        User user = new User();
        user.setUsername("test");
        when(userRepository.existsByUsername(anyString())).thenReturn(true);
        assertThrows(UserAlreadyExistingException.class, () -> userService.save(user));
    }

    @Test
    void should_Return_User_By_Id() {
        User user = new User("testusername", "test test", "USER");
        when(userRepository.findById(1)).thenReturn(Optional.of(user));
        userService.findById(1);
        verify(userRepository).findById(1);
    }

    @Test
    void should_Delete_User_By_Id() {
        doNothing().when(userRepository)
                .deleteById(1);
        userService.deleteById(1);
        verify(userRepository).deleteById(1);
    }

    @Test
    void should_Update_User() throws UserAlreadyExistingException {
        User user = new User("testusername", "test test", "USER");
        user.setId(1);
        when(userRepository.existsByUsername(anyString())).thenReturn(false);
        userService.update(user);
        verify(userRepository).save(user);
    }

    @Test
    void update_Should_Throws_UserAlreadyExistingException() {
        User user1 = new User("username", "test", "ROLE_USER");
        User user2 = new User("user", "test", "ROLE_USER");
        user1.setId(1);
        user2.setId(2);
        when(userRepository.existsByUsername(anyString())).thenReturn(true);
        when(userRepository.findById(anyInt())).thenReturn(Optional.of(user1));
        assertThrows(UserAlreadyExistingException.class, () -> userService.update(user2));
    }
}
