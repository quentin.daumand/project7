package com.nnk.springboot.services;

import com.nnk.springboot.domain.RuleName;
import com.nnk.springboot.repositories.RuleNameRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class RuleNameServiceTest {

    @InjectMocks
    private RuleNameService ruleNameService;

    @Mock
    private RuleNameRepository ruleNameRepository;

    private static List<RuleName> ruleNameList;

    @BeforeEach
    void setUpPerTest() {
        ruleNameList = new ArrayList<>();
        ruleNameList.add(new RuleName("test", "test", "test", "test", "test", "test"));
        ruleNameList.add(new RuleName("test", "test", "test", "test", "test", "test"));
    }

    @Test
    void should_Return_All_RuleNames() {
        when(ruleNameRepository.findAll()).thenReturn(ruleNameList);
        ruleNameService.findAll();
        verify(ruleNameRepository).findAll();
    }

    @Test
    void should_Save_RuleName() {
        RuleName ruleName = new RuleName("test", "test", "test", "test", "test", "test");
        when(ruleNameRepository.save(ruleName)).thenReturn(ruleName);
        ruleNameService.save(ruleName);
        verify(ruleNameRepository).save(ruleName);
    }

    @Test
    void should_Return_RuleName_By_Id() {
        RuleName ruleName = new RuleName("test", "test", "test", "test", "test", "test");
        when(ruleNameRepository.findById(1)).thenReturn(Optional.of(ruleName));
        ruleNameService.findById(1);
        verify(ruleNameRepository).findById(1);
    }

    @Test
    void should_Delete_RuleName_By_Id() {
        doNothing().when(ruleNameRepository)
                .deleteById(1);
        ruleNameService.deleteById(1);
        verify(ruleNameRepository).deleteById(1);
    }

}
