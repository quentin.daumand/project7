package com.nnk.springboot.services;

import com.nnk.springboot.domain.BidList;
import com.nnk.springboot.domain.Trade;
import com.nnk.springboot.repositories.TradeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TradeServiceTest {

    @InjectMocks
    private TradeService tradeService;

    @Mock
    private TradeRepository tradeRepository;

    private static List<Trade> tradeList;

    @BeforeEach
    void setUpPerTest() {
        tradeList = new ArrayList<>();
        tradeList.add(new Trade("test","test",20.00d));
        tradeList.add(new Trade("test","test",30.00d));
    }

    @Test
    void should_Return_All_Trades() {
        when(tradeRepository.findAll()).thenReturn(tradeList);
        tradeService.findAll();
        verify(tradeRepository).findAll();
    }

    @Test
    void should_Save_Trade() {
        Trade trade = new Trade("test","test",30.00d);
        when(tradeRepository.save(trade)).thenReturn(trade);
        tradeService.save(trade);
        verify(tradeRepository).save(trade);
    }

    @Test
    void should_Return_Trade_By_Id() {
        Trade trade = new Trade("test","test",30.00d);
        when(tradeRepository.findById(1)).thenReturn(Optional.of(trade));
        tradeService.findById(1);
        verify(tradeRepository).findById(1);
    }

    @Test
    void should_Delete_Trade_By_Id(){
        doNothing().when(tradeRepository).deleteById(1);
        tradeService.deleteById(1);
        verify(tradeRepository).deleteById(1);
    }

}
