package com.nnk.springboot.services;

import com.nnk.springboot.domain.BidList;
import com.nnk.springboot.repositories.BidListRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class BidListServiceTest {

    @InjectMocks
    private BidListService bidListService;

    @Mock
    private BidListRepository bidListRepository;

    private static List<BidList> bidListList;

    @BeforeEach
    void setUpPerTest() {
        bidListList = new ArrayList<>();
        bidListList.add(new BidList("Test account", "Test type", 10.00d));
        bidListList.add(new BidList("Test account2", "Test type2", 20.00d));
    }

    @Test
    void should_Return_All_BidList() { 
        when(bidListRepository.findAll()).thenReturn(bidListList);
        bidListService.findAll();
        verify(bidListRepository).findAll();
    }

    @Test
    void should_Save_BidList() {
        BidList bidList = new BidList("Test account", "Test type", 10.00d);
        when(bidListRepository.save(bidList)).thenReturn(bidList);
        bidListService.save(bidList);
        verify(bidListRepository).save(bidList);
    }

    @Test
    void should_Return_BidList_By_Id() {
        BidList bidList = new BidList("Test account", "Test type", 10.00d);
        when(bidListRepository.findById(1)).thenReturn(Optional.of(bidList));
        bidListService.findById(1);
        verify(bidListRepository).findById(1);
    }

    @Test
    void should_Delete_BidList_By_Id(){
        doNothing().when(bidListRepository).deleteById(1);
        bidListService.deleteById(1);
        verify(bidListRepository).deleteById(1);
    }
}
