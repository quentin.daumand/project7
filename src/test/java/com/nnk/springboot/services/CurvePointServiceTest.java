package com.nnk.springboot.services;

import com.nnk.springboot.domain.CurvePoint;
import com.nnk.springboot.repositories.CurvePointRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CurvePointServiceTest {

    @InjectMocks
    private CurvePointService curvePointService;

    @Mock
    private CurvePointRepository curvePointRepository;

    private static List<CurvePoint> curvePointList;

    @BeforeEach
    void setUpPerTest() {
        curvePointList = new ArrayList<>();
        curvePointList.add(new CurvePoint(1,14d,10.0d));
        curvePointList.add(new CurvePoint(2,16d,12.0d));
    }

    @Test
    void should_Return_All_CurvePoint() {
        when(curvePointRepository.findAll()).thenReturn(curvePointList);
        curvePointService.findAll();
        verify(curvePointRepository).findAll();
    }

    @Test
    void should_Save_CurvePoint() {
        CurvePoint curvePoint = new CurvePoint(2,16d,12.0d);
        when(curvePointRepository.save(curvePoint)).thenReturn(curvePoint);
        curvePointService.save(curvePoint);
        verify(curvePointRepository).save(curvePoint);
    }

    @Test
    void should_Return_CurvePoint_By_Id() {
        CurvePoint curvePoint = new CurvePoint(2,16d,12.0d);
        when(curvePointRepository.findById(1)).thenReturn(Optional.of(curvePoint));
        curvePointService.findById(1);
        verify(curvePointRepository).findById(1);
    }

    @Test
    void should_Delete_CurvePoint_By_Id(){
        doNothing().when(curvePointRepository).deleteById(1);
        curvePointService.deleteById(1);
        verify(curvePointRepository).deleteById(1);
    }
}
